package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jota.androiddemo.app.Globals;


public class LoginActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Globals.context = getApplicationContext();

        //por tudo no devido lugar



        final TextView txtEmail = (TextView) findViewById(R.id.txtRegisterEmail);
        final TextView txtPassword = (TextView) findViewById(R.id.txtRegisterPassword);

        Button btnLogin = (Button) findViewById(R.id.btnLogin);


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String email = txtEmail.getText().toString();
                String password = txtPassword.getText().toString();
                Bundle dataBundle = new Bundle();

                boolean success = Globals.mgrAuthentication.login(email, password);

                if (success == true) {
                    Toast.makeText(Globals.context, "User logado com sucesso ", Toast.LENGTH_LONG).show();
                    Intent intMenu = new Intent(Globals.context,MenuActivity.class);
                    dataBundle.putLong("idutilizador", Globals.mgrUtilizador.getUser(email, password).getId());
                    intMenu.putExtras(dataBundle);
                    startActivity(intMenu);
                    finish();
                } else {
                    Toast.makeText(Globals.context, "User logado SEM sucesso ", Toast.LENGTH_LONG).show();

                    /**   Toast.makeText(context, "O email e : " + email, Toast.LENGTH_LONG).show();
                     Toast.makeText(context, "A password e : " + password, Toast.LENGTH_LONG).show();
                     */
                }
            }
        });


    }


    public void onBackPressed() {
        // do nothing.
    }

    @Override
    protected void onResume() {
        super.onResume();


        Globals.context = getApplicationContext();


    }


}
