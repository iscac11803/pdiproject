package com.example.jota.androiddemo.managers;

import android.util.Log;

import com.example.jota.androiddemo.model.Evento;
import com.example.jota.androiddemo.model.EventoJogo;
import com.example.jota.androiddemo.persistance.PEventoJogo;

import java.util.List;

/**
 * Created by Jota on 08-06-2015.
 */
public class MgrEventoJogo {

    private static String TAG = "MgrEventosJogo";

    private PEventoJogo pEventoJogo;

    public MgrEventoJogo() {
        pEventoJogo = new PEventoJogo();
    }


    public boolean addEventoJogo(EventoJogo eventojogo) {

        /*Jogador existingJogador = pJogadores.read(jogador.getNome(), jogador.getCategoria(), jogador.getDatanascimento());
*/
        EventoJogo existingEventoJogo = pEventoJogo.read(eventojogo.getIdeventojogo());

        if (existingEventoJogo == null) {
            return pEventoJogo.create(eventojogo);
        } else {
            Log.i(TAG, "The event[" + eventojogo.getIdeventojogo() + "] already exists");
            return false;
        }


    }
    public List<Evento> getAllEventosDoJogo(Long idjogo){
        List<Evento> eventosjogo = pEventoJogo.eventosDoJogo(idjogo);

        Log.d(TAG, "Found ["+eventosjogo.size()+"] events of the selected game");

        return eventosjogo;
    }
}


