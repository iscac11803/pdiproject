package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jota on 24-06-2015.
 */
public class TreinoParser {

    @SerializedName("id")
    private Long id;

    @SerializedName("campo")
    private String campo;

    @SerializedName("categoria")
    private String categoria;

    @SerializedName("localidade")
    private String localidade;

    @SerializedName("hora")
    private String hora;

    @SerializedName("data")
    private String data;
    @SerializedName("observacao")
    private String observacao;

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
