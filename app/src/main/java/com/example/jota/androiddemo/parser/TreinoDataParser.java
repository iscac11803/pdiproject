package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class TreinoDataParser {

    @SerializedName("data")
    private List<TreinoParser> treinos;

    @SerializedName("version")
    private String version;

    @SerializedName("language")
    private String language;


    public List<TreinoParser> getTreinos() {
        return treinos;
    }

    public void setJogo(List<TreinoParser> treinos) {
        this.treinos = treinos;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}