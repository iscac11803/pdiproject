package com.example.jota.androiddemo.model;

/**
 * Created by Jota on 24-06-2015.
 */
public class JogadorTreino {
    Long id, jogadorid, treinoid;

    public JogadorTreino() {
    }

    @Override
    public String toString() {
        return "JogadorTreino{" +
                "id=" + id +
                ", jogadorid=" + jogadorid +
                ", treinoid=" + treinoid +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJogadorid() {
        return jogadorid;
    }

    public void setJogadorid(Long jogadorid) {
        this.jogadorid = jogadorid;
    }

    public Long getTreinoid() {
        return treinoid;
    }

    public void setTreinoid(Long treinoid) {
        this.treinoid = treinoid;
    }
}
