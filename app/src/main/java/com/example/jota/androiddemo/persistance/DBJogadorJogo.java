package com.example.jota.androiddemo.persistance;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.jota.androiddemo.app.Globals;

/**
 * Created by Jota on 04-06-2015.
 */
public class DBJogadorJogo extends SQLiteOpenHelper {

    private static final String TAG = "DBJogadorJogo";

    private static final int VERSION = 1;
    private static final String DB_NAME = "android_demo.db";

//idjogo,idtreinador,campo,resultado,eqAdv,campeonato,escalao,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assoFootball,data,observacao,prestacao

    public DBJogadorJogo(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public static SQLiteDatabase getConnectionToWrite(Context context){

        SQLiteDatabase connection = null;

        DBJogadorJogo dbJogadorJogo = new DBJogadorJogo(Globals.context);

        return dbJogadorJogo.getWritableDatabase();

    }


    public static SQLiteDatabase getConnectionToRead(Context context){

        SQLiteDatabase connection = null;

        DBJogadorJogo dbJogadorJogo = new DBJogadorJogo(Globals.context);

        return dbJogadorJogo.getReadableDatabase();

    }

}
