package com.example.jota.androiddemo.app;

import android.app.Application;
import android.content.Context;

import com.example.jota.androiddemo.managers.MgrAuthentication;
import com.example.jota.androiddemo.managers.MgrEventoJogo;
import com.example.jota.androiddemo.managers.MgrJogador;
import com.example.jota.androiddemo.managers.MgrJogadorJogo;
import com.example.jota.androiddemo.managers.MgrJogadorTreino;
import com.example.jota.androiddemo.managers.MgrJogo;
import com.example.jota.androiddemo.managers.MgrTreino;
import com.example.jota.androiddemo.managers.MgrUtilizador;
/**
 * Created by Jota on 16-05-2015.
 */
public class App extends Application{

    @Override
    public void onCreate() {
        super.onCreate();

        initialize();
    }

    @Override
    public Context getApplicationContext() {
        return super.getApplicationContext();
    }

    private void initialize(){
        Globals.mgrAuthentication = new MgrAuthentication();
        Globals.mgrUtilizador = new MgrUtilizador();
        Globals.mgrJogo = new MgrJogo();
        Globals.mgrJogador = new MgrJogador();
        Globals.mgrJogadorJogo = new MgrJogadorJogo();
        Globals.mgrEventoJogo = new MgrEventoJogo();
        Globals.mgrTreino = new MgrTreino();
        //Initialize other global vars where
        Globals.mgrJogadorTreino = new MgrJogadorTreino();

    }

    @Override
    public void onTerminate() {
        super.onTerminate();



    }
}
