package com.example.jota.androiddemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.managers.MgrJogadorTreino;
import com.example.jota.androiddemo.managers.MgrTreino;
import com.example.jota.androiddemo.model.JogadorTreino;
import com.example.jota.androiddemo.model.Treino;
import com.example.jota.androiddemo.parser.JSONParser;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class FinalizaTreinoActivity extends Activity {

    // JSON Node names
    private static final String TAG_SUCCESS = "success";
    // url to create new product
    private static String url_create_treino = "http://aacsf.atwebpages.com/criartreino.php";
    private static String url_create_jogadortreino = "http://aacsf.atwebpages.com/jogadortreinoinsert.php";
    JSONParser jsonParser = new JSONParser();
    //JSON INICIO
    private ProgressDialog pDialog;
    //JSON FIM
    private String observacao;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finaliza_treino);

        Globals.context = getApplicationContext();
        Button botaofinalizar = (Button) findViewById(R.id.btnfinalizartreino);
        Button botaocancelar = (Button) findViewById(R.id.botaocancelar);
        final EditText observacaotv = (EditText) findViewById(R.id.edittextobservacao);
        Bundle infopassada = getIntent().getExtras();
        final Long idutilizador = infopassada.getLong("idutilizador");
        final Long idtreino = infopassada.getLong("idtreino");
        String categoria = infopassada.getString("categoria");
        String campo = infopassada.getString("campo");
        String localidade = infopassada.getString("localidade");
        String data = infopassada.getString("data");
        String hora = infopassada.getString("hora");
        observacao = observacaotv.getText().toString();

        final Treino treino = new Treino();
        treino.setId(idtreino);
        treino.setData(data);
        treino.setCampo(campo);
        treino.setHora(hora);
        treino.setLocalidade(localidade);
        treino.setCategoria(categoria);


        botaofinalizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) { //tem acesso à internet? Sim? então conclui o treino senão é avisado e não sai dali

                    observacao = observacaotv.getText().toString();
                    treino.setObservacao(observacao);
                    System.out.println("treino " + treino.toString());
                    new CriarTreino(treino).execute();
                    new CriarFaltas(idtreino).execute();

                    AlertDialog alertDialog = new AlertDialog.Builder(
                            FinalizaTreinoActivity.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("Sucesso");

                    // Setting Dialog Message
                    alertDialog.setMessage("Treino criado com sucesso!");

                    // Setting Icon to Dialog
                    alertDialog.setIcon(R.drawable.check);

                    // Setting OK Button
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intMenu = new Intent(Globals.context, MenuActivity.class);
                            Bundle dataBundle = new Bundle();
                            dataBundle.putLong("idutilizador", idutilizador);
                            intMenu.putExtras(dataBundle);
                            startActivity(intMenu);
                            finish();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();


                } else {
                    AlertDialog alertDialog = new AlertDialog.Builder(
                            FinalizaTreinoActivity.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("Ligação à internet");

                    // Setting Dialog Message
                    alertDialog.setMessage("É necessário que tenha acesso à internet para concluir o treino");

                    // Setting Icon to Dialog
                    alertDialog.setIcon(R.drawable.wifi);

                    // Setting OK Button
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                }

            }
        });

        botaocancelar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(FinalizaTreinoActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Apagar treino");

                // Setting Dialog Message
                alertDialog.setMessage("Deseja cancelar a criação deste treino?");

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.apagartreino);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        MgrTreino mgrTreino = new MgrTreino();
                        MgrJogadorTreino mgrJogadorTreino = new MgrJogadorTreino();
                        if (mgrTreino.apagaJogadorDoTreino(treino) && mgrJogadorTreino.apagarJogadoresDoTreino(treino.getId())) {
                            AlertDialog dialog2 = new AlertDialog.Builder(
                                    FinalizaTreinoActivity.this).create();

                            // Setting Dialog Title
                            dialog2.setTitle("Sucesso");

                            // Setting Dialog Message
                            dialog2.setMessage("Treino apagado com sucesso");

                            // Setting Icon to Dialog
                            dialog2.setIcon(R.drawable.check);

                            // Setting OK Button
                            dialog2.setButton("OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intMenu = new Intent(Globals.context, MenuActivity.class);
                                    Bundle dataBundle = new Bundle();
                                    dataBundle.putLong("idutilizador", idutilizador);
                                    intMenu.putExtras(dataBundle);
                                    startActivity(intMenu);
                                    finish();
                                }
                            });

                            // Showing Alert Message
                            dialog2.show();

                        }
                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();
            }
        });

    }


    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public void onBackPressed() {
        // do nothing.
    }

    class CriarTreino extends AsyncTask<String, String, String> {
        private Treino treinoJson;

        public CriarTreino(Treino treino) {
            treinoJson = new Treino();
            this.treinoJson = treino;
        }

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(FinalizaTreinoActivity.this);
            pDialog.setMessage("A criar treino..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {

            String categoria = treinoJson.getCategoria();
            String campo = treinoJson.getCampo();
            String hora = treinoJson.getHora();
            String data = treinoJson.getData();
            String localidade = treinoJson.getLocalidade();
            String observacao = treinoJson.getObservacao();
            //List<JogadorTreino> jogadorTreinoList =Globals.mgrJogadorTreino.getJogadoresDoTreino(idTreinoJson);


            // Building Parameters
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(new BasicNameValuePair("campo", campo));
            params.add(new BasicNameValuePair("categoria", categoria));
            params.add(new BasicNameValuePair("localidade", localidade));
            params.add(new BasicNameValuePair("hora", hora));
            params.add(new BasicNameValuePair("data", data));
            params.add(new BasicNameValuePair("observacao", observacao));


            // getting JSON Object
            // Note that create product url accepts POST method
            JSONObject json = jsonParser.makeHttpRequest(url_create_treino,
                    "POST", params);

            // check log cat fro response
            Log.d("Create Response", json.toString());

            // check for success tag
            try {
                int success = json.getInt(TAG_SUCCESS);

                if (success == 1) {
                    // successfully created product
                    System.out.println("Treino inserido com sucesso");
                    // closing this screen

                } else {
                    // failed to create product
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */
        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }

    class CriarFaltas extends AsyncTask<String, String, String> {
        private Long idTreinoJson;

        public CriarFaltas(Long idtreino) {
            this.idTreinoJson = idtreino;
        }

        /**
         * Before starting background thread Show Progress Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            pDialog = new ProgressDialog(FinalizaTreinoActivity.this);
            pDialog.setMessage("A criar treino..");
            pDialog.setIndeterminate(false);
            pDialog.setCancelable(true);
            pDialog.show();
        }

        /**
         * Creating product
         */
        protected String doInBackground(String... args) {
            System.out.println("idtreinopassado: " + idTreinoJson);
            List<JogadorTreino> jogadorTreinoList = Globals.mgrJogadorTreino.getJogadoresDoTreino(idTreinoJson);


            for (int i = 0; i < jogadorTreinoList.size(); i++) {
                // Building Parameters

                List<NameValuePair> params = new ArrayList<NameValuePair>();
                JogadorTreino jogadorTreino = new JogadorTreino();
                jogadorTreino.setTreinoid(jogadorTreinoList.get(i).getTreinoid());
                jogadorTreino.setJogadorid(jogadorTreinoList.get(i).getJogadorid());

                System.out.println("idtreino: " + jogadorTreino.getTreinoid());
                System.out.println("idjogador: " + jogadorTreino.getJogadorid());
                params.add(new BasicNameValuePair("treinoid", jogadorTreino.getTreinoid().toString()));
                params.add(new BasicNameValuePair("jogadorid", jogadorTreino.getJogadorid().toString()));

                JSONObject json = jsonParser.makeHttpRequest(url_create_jogadortreino,
                        "POST", params);
                // check log cat fro response
                Log.d("Create Response", json.toString());
                // check for success tag
                try {
                    int success = json.getInt(TAG_SUCCESS);

                    if (success == 1) {
                        // successfully created product
                        System.out.println("Jogador inserido com sucesso");
                        // closing this screen

                    } else {
                        // failed to create product
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
            return null;
        }

        /**
         * After completing background task Dismiss the progress dialog
         * *
         */

        protected void onPostExecute(String file_url) {
            // dismiss the dialog once done
            pDialog.dismiss();
        }

    }
}
