package com.example.jota.androiddemo.persistance;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.jota.androiddemo.app.Globals;

/**
 * Created by Jota on 24-06-2015.
 */
public class DBJogadorTreino extends SQLiteOpenHelper {

    private static final String TAG = "DBJogadorTreino";

    private static final int VERSION = 1;
    private static final String DB_NAME = "android_demo.db";


    public DBJogadorTreino(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    public static SQLiteDatabase getConnectionToWrite(Context context) {

        SQLiteDatabase connection = null;

        DBJogadorTreino dbJogadorTreino = new DBJogadorTreino(Globals.context);

        return dbJogadorTreino.getWritableDatabase();

    }

    public static SQLiteDatabase getConnectionToRead(Context context) {

        SQLiteDatabase connection = null;

        DBJogadorTreino dbJogadorTreino = new DBJogadorTreino(Globals.context);

        return dbJogadorTreino.getReadableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
