package com.example.jota.androiddemo.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Treino;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class PTreino {

    private static final String TAG = "PTreinos";

    private static final String TABLE_NAME = "treino";

    public PTreino() {
    }

    public boolean create(Treino treino) {

        SQLiteDatabase connection = null;

        try {

            connection = DBTreino.getConnectionToWrite(Globals.context);
            ContentValues values = new ContentValues();


            values.put("id", treino.getId());
            values.put("campo", treino.getCampo());
            values.put("categoria", treino.getCategoria());
            values.put("localidade", treino.getLocalidade());
            values.put("hora", treino.getHora());
            values.put("data", treino.getData());
            values.put("searchdata", treino.getSearchdata());
            values.put("observacao", treino.getObservacao());


            long insertedId = connection.insert(TABLE_NAME, null, values);

            if (insertedId <= 0) {
                Log.e(PTreino.class.getName(), "Error while inserting the training");
                return false;
            } else {
                treino.setId(insertedId);
            }

            return true;

        } catch (Exception ex) {
            Log.e(TAG, "An error while creating training", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    public Treino read(String data, String categoria, String hora, String localidade, String campo) {

        SQLiteDatabase connection = DBTreino.getConnectionToRead(Globals.context);


        Cursor cursor = connection.rawQuery("SELECT rowid as _id,* FROM treino WHERE categoria=? AND data=? AND hora=? AND campo=? AND localidade=?",
                new String[]{categoria, data, hora, campo, localidade});

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {

                    Long idtreinoDb = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                    String categoriaDb = cursor.getString(cursor.getColumnIndexOrThrow("categoria"));
                    String horaDb = cursor.getString(cursor.getColumnIndexOrThrow("hora"));
                    String dataDb = cursor.getString(cursor.getColumnIndexOrThrow("data"));
                    String localidadeDb = cursor.getString(cursor.getColumnIndexOrThrow("localidade"));
                    String campoDb = cursor.getString(cursor.getColumnIndexOrThrow("campo"));
                    String observacaoDb = cursor.getString(cursor.getColumnIndex("observacao"));

                    Treino treino = new Treino();
                    treino.setId(idtreinoDb);
                    treino.setCategoria(categoriaDb);
                    treino.setHora(horaDb);
                    treino.setData(dataDb);
                    treino.setLocalidade(localidadeDb);
                    treino.setCampo(campoDb);
                    treino.setObservacao(observacaoDb);

                    return treino;
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

        return null;
    }

    public boolean update(Treino treino) {

        SQLiteDatabase connection = null;

        try {

            connection = DBTreino.getConnectionToWrite(Globals.context);

            ContentValues values = new ContentValues();

            if (treino.getId() != null) {
                System.out.println("id do treino criado:" + treino.getId());
                values.put("id", treino.getId());
                values.put("campo", treino.getCampo());
                values.put("categoria", treino.getCategoria());
                values.put("localidade", treino.getLocalidade());
                values.put("hora", treino.getHora());
                values.put("data", treino.getData());
                values.put("observacao", treino.getObservacao());
            }


            long numRowAffected = connection.update(TABLE_NAME, values, "rowid = ?", new String[]{String.valueOf(treino.getId())});

            if (numRowAffected > 0) {
                return true;
            }

            return false;

        }catch (Exception ex){
            Log.e(TAG, "An error while updating training", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public boolean delete(Treino treino) {
        SQLiteDatabase connection = null;
        System.out.println("treino no ptreino: " + treino.toString());
        try {

            connection = DBTreino.getConnectionToWrite(Globals.context);

            long numRowsAffected = connection.delete(TABLE_NAME, "rowid = ?", new String[]{String.valueOf(treino.getId())});

            if (numRowsAffected > 0) {
                return true;
            }

            return false;

        }catch (Exception ex){
            Log.e(TAG, "An error while deleting the training", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    public List<Treino> readAll() {

        SQLiteDatabase connection = DBTreino.getConnectionToRead(Globals.context);

        Cursor cursor = connection.rawQuery("SELECT * FROM treino ", null);

        List<Treino> treinos = new ArrayList();

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        Long idtreinoDb = cursor.getLong(cursor.getColumnIndex("id"));
                        String campoDb = cursor.getString(cursor.getColumnIndex("campo"));
                        String categoriaDb = cursor.getString(cursor.getColumnIndex("categoria"));
                        String localidadeDb = cursor.getString(cursor.getColumnIndex("localidade"));
                        String horaDb = cursor.getString(cursor.getColumnIndex("hora"));
                        String dataDb = cursor.getString(cursor.getColumnIndex("data"));
                        String observacaoDb = cursor.getString(cursor.getColumnIndex("observacao"));


                        Treino treino = new Treino();
                        treino.setId(idtreinoDb);
                        treino.setCampo(campoDb);
                        treino.setCategoria(categoriaDb);
                        treino.setLocalidade(localidadeDb);
                        treino.setHora(horaDb);
                        treino.setData(dataDb);
                        treino.setObservacao(observacaoDb);


                        treinos.add(treino);
                        cursor.moveToNext();
                    }
                }
            }

            return treinos;

        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

    }

    public List<Treino> readAllByDate() {
        SQLiteDatabase connection = DBTreino.getConnectionToRead(Globals.context);


        Cursor cursor = connection.rawQuery("SELECT * FROM treino ORDER BY data DESC", null);

        List<Treino> treinos = new ArrayList();

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        Long idtreinoDb = cursor.getLong(cursor.getColumnIndex("id"));
                        String campoDb = cursor.getString(cursor.getColumnIndex("campo"));
                        String categoriaDb = cursor.getString(cursor.getColumnIndex("categoria"));
                        String localidadeDb = cursor.getString(cursor.getColumnIndex("localidade"));
                        String horaDb = cursor.getString(cursor.getColumnIndex("hora"));
                        String dataDb = cursor.getString(cursor.getColumnIndex("data"));
                        String observacaoDb = cursor.getString(cursor.getColumnIndex("observacao"));


                        Treino treino = new Treino();
                        treino.setId(idtreinoDb);
                        treino.setCampo(campoDb);
                        treino.setCategoria(categoriaDb);
                        treino.setLocalidade(localidadeDb);
                        treino.setHora(horaDb);
                        treino.setData(dataDb);
                        treino.setObservacao(observacaoDb);


                        treinos.add(treino);
                        cursor.moveToNext();
                    }
                }
            }

            return treinos;

        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

    }


    public void apagaTreino(Long idtreino) {

    }
}


