package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jota on 04-06-2015.
 */
public class JogadorJogoDataParser {

    @SerializedName("data")
    private List<JogadorJogoParser> jogadoresdojogo;

    @SerializedName("version")
    private String version;

    @SerializedName("language")
    private String language;





    public void setJogador(List<JogadorJogoParser> jogadordojogo) {
        this.jogadoresdojogo = jogadordojogo;
    }
    public List<JogadorJogoParser> getJogadoresDoJogo() {
        return jogadoresdojogo;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
