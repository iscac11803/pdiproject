package com.example.jota.androiddemo.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.JogadorJogo;

/**
 * Created by Jota on 04-06-2015.
 */
public class PJogadorJogo {
    private static final String TAG = "PJogadoresJogo";

    private static final String TABLE_NAME = "jogadorjogo";

    public JogadorJogo read(Long idjogadorjogo) {

        SQLiteDatabase connection = DBJogadorJogo.getConnectionToRead(Globals.context);


        Cursor cursor = connection.
                rawQuery("SELECT * FROM jogadorjogo WHERE rowid=?",
                        new String[]{idjogadorjogo.toString()});

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {


                    Long idjogadorjogoDb = cursor.getLong(cursor.getColumnIndex("id"));





                    JogadorJogo jogadorjogo = new JogadorJogo();
                    jogadorjogo.setId(idjogadorjogoDb);



                    return jogadorjogo;
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

        return null;
    }
    public boolean create(JogadorJogo jogadorjogo) {

        SQLiteDatabase connection = null;

        try {

            connection = DBJogadorJogo.getConnectionToWrite(Globals.context);
            ContentValues values = new ContentValues();

            values.put("id", jogadorjogo.getJogadorid());
            values.put("jogadorid", jogadorjogo.getJogadorid());
            values.put("jogoid", jogadorjogo.getJogoid());
            values.put("idevento", jogadorjogo.getIdevento());

            values.put("observacao", jogadorjogo.getObservacao());
            values.put("prestacao", jogadorjogo.getPrestacao());






            long insertedId = connection.insert(TABLE_NAME, null, values);

            if (insertedId <= 0) {
                Log.e(PJogador.class.getName(), "Error while inserting the game");
                return false;
            } else {
                jogadorjogo.setJogadorid(insertedId);
            }

            return true;

        }catch (Exception ex){
            Log.e(TAG, "An error while creating player", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
}
