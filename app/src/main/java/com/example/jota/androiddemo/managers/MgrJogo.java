package com.example.jota.androiddemo.managers;

import android.util.Log;

import com.example.jota.androiddemo.model.Jogo;
import com.example.jota.androiddemo.persistance.PJogo;

import java.util.List;

/**
 * Created by Jota on 20-05-2015.
 */
public class MgrJogo {

    private static String TAG = "MgrJogos";

    private PJogo pJogo;

    public MgrJogo() {
        pJogo = new PJogo();
    }

    public Jogo getJogo(String data, String categoria, String visitado, String visitante, String resultado, String campo) {


        Jogo jogo = pJogo.read(data, categoria, visitado, visitante, resultado, campo);

        return jogo;

    }

    public boolean addJogo(Jogo jogo){

        Jogo existingJogos = pJogo.read(jogo.getData(), jogo.getCategoria(), jogo.getVisitado(), jogo.getVisitante(), jogo.getResultado(), jogo.getCampo());

        if (existingJogos == null){
            return pJogo.create(jogo);
        }else {
            Log.i(TAG, "The game[" + jogo.getId() + "] already exists");
            return false;
        }
//jogo.getTreinadorid(),jogo.getCampo(),jogo.getResultado(),jogo.getEqAdv(),jogo.getCampeonato(),jogo.getCategoria(),jogo.getSerie(),jogo.getLocalidade(),jogo.getHora(),jogo.getJornada(),jogo.getVisitado(),jogo.getVisitante(),jogo.getArbitro1(),jogo.getArbitro2(),jogo.getAssoFootball(),jogo.getData(),jogo.getObservacao(),jogo.getPrestacao()

    }

    public List<Jogo> getAllJogos() {

        List<Jogo> jogos = pJogo.readAll();

        Log.d(TAG, "Found ["+jogos.size()+"] games");

        return jogos;

    }
    public List<Jogo> getAllJogoByDate(){

        List<Jogo> jogos = pJogo.readAllByDate();
            Log.d(TAG, "Found ["+jogos.size()+"] games");
        return jogos;

    }


}

