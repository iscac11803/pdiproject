package com.example.jota.androiddemo.model;

/**
 * Created by Jota on 20-05-2015.
 */
public class Jogo {
    private Long id, treinadorid;
    private String campo;
    private String resultado;
    private String eqAdv;
    private String campeonato;
    private String categoria;
    private String serie;
    private String hora;
    private String jornada;
    private String visitado;
    private String visitante;
    private String arbitro1;
    private String arbitro2;
    private String assoFootball;
    private String data;
    private String observacao;
    private String prestacao;
    private String conclusao;
    private String searchdata;
    private String localidade;

    public Jogo() {
    }

    public String getConclusao() {
        return conclusao;
    }

    public void setConclusao(String conclusao) {
        this.conclusao = conclusao;
    }

    public String getSearchdata() {
        return searchdata;
    }

    public void setSearchdata(String searchdata) {
        this.searchdata = searchdata;
    }

public String dadosJogo(){

    return "Campo"+campo+"\n"+"Visitado "+visitado+"\n"+"Visitante "+visitante+"\n"+"Resultado "+resultado+"\n"+"Data "+data+"\n"+"Categoria "+categoria;
}

    @Override
    public String toString() {
        return "Jogo{" +
                "id=" + id +
                ", treinadorid=" + treinadorid +
                ", campo='" + campo + '\'' +
                ", resultado='" + resultado + '\'' +
                ", eqAdv='" + eqAdv + '\'' +
                ", campeonato='" + campeonato + '\'' +
                ", categoria='" + categoria + '\'' +
                ", serie='" + serie + '\'' +
                ", localidade='" + localidade + '\'' +
                ", hora='" + hora + '\'' +
                ", jornada='" + jornada + '\'' +
                ", visitado='" + visitado + '\'' +
                ", visitante='" + visitante + '\'' +
                ", arbitro1='" + arbitro1 + '\'' +
                ", arbitro2='" + arbitro2 + '\'' +
                ", assoFootball='" + assoFootball + '\'' +
                ", data='" + data + '\'' +
                ", observacao='" + observacao + '\'' +
                ", prestacao='" + prestacao + '\'' +
                ", conclusao='" + conclusao + '\'' +
                ", searchdata='" + searchdata + '\'' +
                ", localidade='" + localidade + '\'' +
                '}';
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTreinadorid() {
        return treinadorid;
    }

    public void setTreinadorid(Long treinadorid) {
        this.treinadorid = treinadorid;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getEqAdv() {
        return eqAdv;
    }

    public void setEqAdv(String eqAdv) {
        this.eqAdv = eqAdv;
    }

    public String getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(String campeonato) {
        this.campeonato = campeonato;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getJornada() {
        return jornada;
    }

    public void setJornada(String jornada) {
        this.jornada = jornada;
    }

    public String getVisitado() {
        return visitado;
    }

    public void setVisitado(String visitado) {
        this.visitado = visitado;
    }

    public String getVisitante() {
        return visitante;
    }

    public void setVisitante(String visitante) {
        this.visitante = visitante;
    }

    public String getArbitro1() {
        return arbitro1;
    }

    public void setArbitro1(String arbitro1) {
        this.arbitro1 = arbitro1;
    }

    public String getArbitro2() {
        return arbitro2;
    }

    public void setArbitro2(String arbitro2) {
        this.arbitro2 = arbitro2;
    }

    public String getAssoFootball() {
        return assoFootball;
    }

    public void setAssoFootball(String assoFootball) {
        this.assoFootball = assoFootball;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getPrestacao() {
        return prestacao;
    }

    public void setPrestacao(String prestacao) {
        this.prestacao = prestacao;
    }
}
