package com.example.jota.androiddemo.persistance;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class JogoDbAdapter {

    public static final String ID_JOGO = "id";
    public static final String ID_TREINADOR = "treinadorid";
    public static final String CAMPO = "campo";
    public static final String RESULTADO = "resultado";
    public static final String EQ_ADV = "eqadv";
    public static final String CAMPEONATO = "campeonato";
    public static final String CATEGORIA = "categoria";
    public static final String SERIE = "serie";
    public static final String LOCALIDADE = "localidade";
    public static final String HORA = "hora";
    public static final String JORNADA = "jornada";
    public static final String VISITADO = "visitado";
    public static final String VISITANTE = "visitante";
    public static final String ARBITRO1 = "arbitro1";
    public static final String ARBITRO2 = "arbitro2";
    public static final String ASSO_FOOTBALL = "assofootball";
    public static final String DATA = "data";
    public static final String OBSERVACAO = "observacao";
    public static final String PRESTACAO = "prestacao";
    public static final String CONCLUSAO = "conclusao";
    public static final String KEY_SEARCH = "keysearch";

    private static final String TAG = "CustomersDbAdapter";
    private static final String DATABASE_NAME = "android_demo.db";
    private static final String FTS_VIRTUAL_TABLE = "jogo";
    private static final int DATABASE_VERSION = 1;
    //Create a FTS3 Virtual Table for fast searches
    private static final String DATABASE_CREATE =
            "CREATE VIRTUAL TABLE " + FTS_VIRTUAL_TABLE + " USING fts4(" +
                    ID_JOGO + "," +
                    ID_TREINADOR + "," +
                    CAMPO + "," +
                    RESULTADO + "," +
                    EQ_ADV + "," +
                    CAMPEONATO + "," +
                    CATEGORIA + "," +
                    SERIE + "," +
                    LOCALIDADE + "," +
                    HORA + "," +
                    JORNADA + "," +
                    VISITADO + "," +
                    VISITANTE + "," +
                    ARBITRO1 + "," +
                    ARBITRO2 + "," +
                    ASSO_FOOTBALL + "," +
                    DATA + "," +
                    OBSERVACAO + "," +
                    PRESTACAO + "," +
                    CONCLUSAO + "," +
                    KEY_SEARCH + "," +
                    " UNIQUE (" + ID_JOGO + "));";
    private final Context mCtx;
    //campo,resultado,eqAdv,campeonato, categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assoFootball,data,observacao,prestacao
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    public JogoDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public JogoDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }






    public Cursor searchJogo(String inputText) throws SQLException {
        Log.w(TAG, inputText);

        String query = "SELECT rowid as _id,* from jogo where searchdata MATCH '" + inputText + "'";


        Log.w(TAG, query);
        Cursor mCursor = mDb.rawQuery(query,null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }


    public boolean deleteAllJogos() {

        int doneDelete = 0;
        doneDelete = mDb.delete(FTS_VIRTUAL_TABLE, null , null);
        Log.w(TAG, Integer.toString(doneDelete));
        return doneDelete > 0;

    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            /*Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);*/
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            /*Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + FTS_VIRTUAL_TABLE);
            onCreate(db);*/
        }
    }

}