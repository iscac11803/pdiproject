package com.example.jota.androiddemo.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.commons.GenericRestClient;
import com.example.jota.androiddemo.model.JogadorJogo;
import com.example.jota.androiddemo.parser.JogadorJogoDataParser;
import com.example.jota.androiddemo.parser.JogadorJogoParser;
import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 04-06-2015.
 */
public class JogadorDoJogoLoader extends AsyncTask<String, Integer, Boolean> {

    private final String TAG = "JogadoresDoJogoLoader";
    private final Context context;


    public JogadorDoJogoLoader(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... params) {
        //Faz coisas em background
        List<JogadorJogo> jogadoresdojogo = new ArrayList();

        String serviceUrl = params[0];

        GenericRestClient rest = new GenericRestClient();
        rest.setLocation(serviceUrl);

        try {
            InputStreamReader result = rest.invoke();
            if (result != null) {
                Gson parser = new Gson();
                JogadorJogoDataParser resultParsed = parser.fromJson(result, JogadorJogoDataParser.class);
                if (resultParsed != null) {
                    if (resultParsed.getJogadoresDoJogo() != null) {
                        for (JogadorJogoParser jogadorjogoParser : resultParsed.getJogadoresDoJogo()) {
                            // idjogo,idtreinador, campo,resultado,eqAdv,campeonato, categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assoFootball,data,observacao,prestacao;
                            JogadorJogo jogadorjogo = new JogadorJogo();
                            jogadorjogo.setId(jogadorjogoParser.getId());
                            jogadorjogo.setJogadorid(jogadorjogoParser.getJogadorid());
                            jogadorjogo.setJogoid(jogadorjogoParser.getJogoid());
                            jogadorjogo.setIdevento(jogadorjogoParser.getIdevento());
                            jogadorjogo.setObservacao(jogadorjogoParser.getObservacao());
                            jogadorjogo.setPrestacao(jogadorjogoParser.getPrestacao());

                            jogadoresdojogo.add(jogadorjogo);
                        }
                    }
                    if (jogadoresdojogo != null && jogadoresdojogo.size() > 0) {
                        for (JogadorJogo jogadorJogo : jogadoresdojogo) {

                            boolean addResult = Globals.mgrJogadorJogo.addJogadorJogo(jogadorJogo);
                            if (addResult == false) {
                                Log.e(TAG, "Error while adding game[" + jogadorJogo.getJogadorid() + "]");
                            }
                        }

                        return true;
                    }
                }
            }

            return false;

        } catch (Exception e) {
            Log.e(TAG, "Error while loading train stations from service [" + Globals.SERVICE_JOGADORES_URL + "]", e);
            return null;
        }
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        //Fazer qualquer coisa depois de terminar a tarefa
        // (util para iniciar uma caixa de progresso)
    }

}
