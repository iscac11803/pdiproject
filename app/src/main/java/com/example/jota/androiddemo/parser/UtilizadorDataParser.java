package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jota on 18-05-2015.
 */
public class UtilizadorDataParser {
//TODO fazer o jogodataparser e o jogoparser
    @SerializedName("data")
    private List<UtilizadorParser> utilizadores;

    @SerializedName("version")
    private String version;

    @SerializedName("language")
    private String language;


    public List<UtilizadorParser> getUtilizadores() {
        return utilizadores;
    }

    public void setUtilizadores(List<UtilizadorParser> utilizadores) {
        this.utilizadores = utilizadores;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
