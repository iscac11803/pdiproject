package com.example.jota.androiddemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jota.androiddemo.R;
import com.example.jota.androiddemo.model.Utilizador;

import java.util.List;


/**
 * Created by Pedro on 05/05/2015.
 */
public class UtilizadorAdapter extends BaseAdapter {

    private Context context;
    private List<Utilizador> utilizadors;

    public UtilizadorAdapter(Context context, List<Utilizador> utilizadors) {
        super();
        this.context = context;
        this.utilizadors = utilizadors;
    }

    @Override
    public int getCount() {
        return utilizadors.size();
    }

    @Override
    public Object getItem(int position) {
        return utilizadors.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater mInflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_user_item, parent,false);
        }

        TextView elementEmail= (TextView)convertView.findViewById(R.id.listUserEmail);

        Utilizador utilizador = utilizadors.get(position);
        elementEmail.setText(utilizador.getEmail());



        return convertView;
    }
}

