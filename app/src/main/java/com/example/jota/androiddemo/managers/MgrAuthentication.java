package com.example.jota.androiddemo.managers;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Utilizador;

/**
 * Created by Jota on 23-04-2015.
 */
public class MgrAuthentication {


    public boolean login(String email, String password) {

        Utilizador u = new Utilizador();
        u.setEmail(email);
        u.setSenha(password);

//        Globals.mgrUsers.addUser(context, u);

        Utilizador utilizador = Globals.mgrUtilizador.getUser(email, password);

        if (utilizador != null) {
            return true;
        }else{

            //Seria aqui que meter�amos a l�gica das tentativas de login (ex: m�x 3 tentativa bloquea a conta)
            return false;
        }

    }

    public boolean signUp(Utilizador newUtilizador) {

        return Globals.mgrUtilizador.addUser(newUtilizador);

    }



}