package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.example.jota.androiddemo.app.Globals;

public class DadosTreinoActivity extends Activity {
    private TextView txtcampo, txtdata, txtcategoria, txtlocalidade, txthora;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_treino);
        Globals.context = getApplicationContext();


        Bundle infopassada = getIntent().getExtras();

        String campo = infopassada.getString("campo");
        String data = infopassada.getString("data");
        String localidade = infopassada.getString("localidade");
        String hora = infopassada.getString("hora");
        String categoria = infopassada.getString("categoria");
        String observacao = infopassada.getString("observacao");
        final Long idtreino = infopassada.getLong("idtreino");

        txtcampo = (TextView) findViewById(R.id.dadostreinocampo);
        txtdata = (TextView) findViewById(R.id.dadostreinodata);
        txthora = (TextView) findViewById(R.id.dadostreinohora);
        txtcategoria = (TextView) findViewById(R.id.dadostreinocategoria);
        txtlocalidade = (TextView) findViewById(R.id.dadostreinolocalidade);
        TextView txtobservacao = (TextView) findViewById(R.id.txtobservacao);

        txtcampo.setText(campo);
        txtdata.setText(data);
        txtcategoria.setText(categoria);
        txtlocalidade.setText(localidade);
        txthora.setText(hora);
        txtobservacao.setText(observacao);

        Button botaoJogadores = (Button) findViewById(R.id.btnIdJogadoresTreino);
        botaoJogadores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle dataBundle = new Bundle();
                Intent intMenu = new Intent(Globals.context, JogadoresDoTreinoActivity.class);
                dataBundle.putLong("idtreino", idtreino);
                intMenu.putExtras(dataBundle);
                startActivity(intMenu);
            }
        });


    }


}