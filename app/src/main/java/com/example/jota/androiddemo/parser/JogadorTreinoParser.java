package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jota on 24-06-2015.
 */
public class JogadorTreinoParser {
    @SerializedName("id")
    private Long id;

    @SerializedName("jogadorid")
    private Long jogadorid;

    @SerializedName("treinoid")
    private Long treinoid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJogadorid() {
        return jogadorid;
    }

    public void setJogadorid(Long jogadorid) {
        this.jogadorid = jogadorid;
    }

    public Long getTreinoid() {
        return treinoid;
    }

    public void setTreinoid(Long treinoid) {
        this.treinoid = treinoid;
    }
}
