package com.example.jota.androiddemo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jota.androiddemo.R;
import com.example.jota.androiddemo.model.Treino;

import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class TreinoAdapter extends BaseAdapter {

    private Context context;
    private List<Treino> treinos;

    public TreinoAdapter(Context context, List<Treino> treinos) {
        super();
        this.context = context;
        this.treinos = treinos;
    }


    @Override
    public int getCount() {
        return treinos.size();
    }

    @Override
    public Object getItem(int position) {
        return treinos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return treinos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_treinos_item, parent, false);
        }
        TextView elementCampo = (TextView) convertView.findViewById(R.id.listTreinoCampo);
        TextView elementData = (TextView) convertView.findViewById(R.id.listTreinoData);
        TextView elementCategoria = (TextView) convertView.findViewById(R.id.listTreinoCategoria);
        TextView elementHora = (TextView) convertView.findViewById(R.id.listTreinoHora);
        TextView elementLocalidade = (TextView) convertView.findViewById(R.id.listTreinoLocalidade);

        elementCampo.setFocusable(false);
        elementLocalidade.setFocusable(false);
        elementHora.setFocusable(false);
        elementData.setFocusable(false);
        elementCategoria.setFocusable(false);

        elementData.setFocusableInTouchMode(false);
        elementCategoria.setFocusableInTouchMode(false);
        elementHora.setFocusableInTouchMode(false);
        elementLocalidade.setFocusableInTouchMode(false);
        elementCampo.setFocusableInTouchMode(false);

        Treino treino = treinos.get(position);

        elementData.setText(treino.getData());
        elementCategoria.setText(treino.getCategoria());
        elementHora.setText(treino.getHora());
        elementLocalidade.setText(treino.getLocalidade());
        elementCampo.setText(treino.getCampo());


        switch (treino.getCategoria()) {
            case "Sénior":
                convertView.setBackgroundColor(Color.parseColor("#FFFF090C"));
                break;
            case "Júnior A":
                convertView.setBackgroundColor(Color.parseColor("#FFFF9824"));
                break;
            case "Júnior B":
                convertView.setBackgroundColor(Color.parseColor("#FFFF9824"));
                break;
            case "Júnior C":
                convertView.setBackgroundColor(Color.parseColor("#FFFF9824"));
                break;
            case "Júnior D":
                convertView.setBackgroundColor(Color.parseColor("#FFFF9824"));
                break;
            case "Benjamim":
                convertView.setBackgroundColor(Color.parseColor("#FFFFF824"));
                break;
            case "Traquina":
                convertView.setBackgroundColor(Color.parseColor("#FFBBFF19"));
                break;
            case "Petiz":
                convertView.setBackgroundColor(Color.parseColor("#FF22FFF5"));
                break;
            default:
                System.out.println("Categoria n reconhecida");

        }

        return convertView;
    }

}


