package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jota.androiddemo.adapter.JogoAdapter;
import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Jogo;

import java.util.List;


public class JogosActivityList extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jogos);
        Bundle extras = getIntent().getExtras();
        final Long idutilizador = extras.getLong("idutilizador");

        Globals.context = getApplicationContext();

        final ListView listJogos = (ListView) findViewById(R.id.list);

        List<Jogo> jogos = Globals.mgrJogo.getAllJogoByDate();
        JogoAdapter adapter = new JogoAdapter(Globals.context, jogos);
        listJogos.setAdapter(adapter);


        //setListAdapter(new JogosAdapter(this, jogos));

        Button botaoPesqAvan = (Button) findViewById(R.id.btnjogospesqavan);
        botaoPesqAvan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle dataBundle = new Bundle();
                Intent intMenu = new Intent(Globals.context, SearchViewJogosActivity.class);
                dataBundle.putLong("idutilizador", idutilizador);
                intMenu.putExtras(dataBundle);
                startActivity(intMenu);
            }
        });


        listJogos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {


                //idjogo,idtreinador,campo,resultado,eqAdv,campeonato,categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assofootball,data,observacao,prestacao

                TextView visitadotv = ((TextView) view.findViewById(R.id.listJogoVisitado));
                TextView visitantetv = ((TextView) view.findViewById(R.id.listJogoVisitante));
                TextView resultadotv = ((TextView) view.findViewById(R.id.listJogoResultado));
                TextView datatv = ((TextView) view.findViewById(R.id.listJogoData));
                TextView categoriatv = ((TextView) view.findViewById(R.id.listJogoCategoria));
                TextView campotv = ((TextView) view.findViewById(R.id.listJogoCampo));

                String visitado = visitadotv.getText().toString();
                String visitante = visitantetv.getText().toString();
                String resultado = resultadotv.getText().toString();
                String data = datatv.getText().toString();
                String categoria = categoriatv.getText().toString();
                String campo = campotv.getText().toString();


                Log.v("MyApp", "get onItem Click position= " + position);


                Jogo jogo = Globals.mgrJogo.getJogo(data, categoria, visitado, visitante, resultado, campo);


                Bundle dataBundle = new Bundle();


                dataBundle.putString("campo", jogo.getCampo());
                dataBundle.putString("resultado", jogo.getResultado());
                dataBundle.putString("visitado", jogo.getVisitado());
                dataBundle.putString("visitante", jogo.getVisitante());
                dataBundle.putString("data", jogo.getData());
                dataBundle.putString("categoria", jogo.getCategoria());
                dataBundle.putString("eqadv", jogo.getEqAdv());
                dataBundle.putString("campeonato", jogo.getCampeonato());
                dataBundle.putString("serie", jogo.getSerie());
                dataBundle.putString("localidade", jogo.getLocalidade());
                dataBundle.putString("hora", jogo.getHora());
                dataBundle.putString("jornada", jogo.getJornada());
                dataBundle.putString("arbitro1", jogo.getArbitro1());
                dataBundle.putString("arbitro2", jogo.getArbitro2());
                dataBundle.putString("assofootball", jogo.getAssoFootball());
                dataBundle.putString("observacao", jogo.getObservacao());
                dataBundle.putString("prestacao", jogo.getPrestacao());
                dataBundle.putString("conclusao", jogo.getConclusao());
                System.out.println(jogo.getId());
                dataBundle.putLong("idjogo", jogo.getId());

                // dataBundle.putLong("idtreinador", jogo.getTreinadorid());
                // dataBundle.putLong("idjogo", jogo.getId());

                Intent intMain = new Intent(Globals.context, DadosJogoActivity.class);
                intMain.putExtras(dataBundle);
                startActivity(intMain);
                finish();


            }
        });

    }


    }



