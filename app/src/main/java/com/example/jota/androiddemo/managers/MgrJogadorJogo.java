package com.example.jota.androiddemo.managers;

import android.util.Log;

import com.example.jota.androiddemo.model.JogadorJogo;
import com.example.jota.androiddemo.persistance.PJogadorJogo;

/**
 * Created by Jota on 04-06-2015.
 */
public class MgrJogadorJogo {

    private static String TAG = "MgrJogadores";

    private PJogadorJogo pJogadorJogo;

    public MgrJogadorJogo() {
        pJogadorJogo = new PJogadorJogo();
    }



    public boolean addJogadorJogo(JogadorJogo jogadorjogo){

        /*Jogador existingJogador = pJogadores.read(jogador.getNome(), jogador.getCategoria(), jogador.getDatanascimento());
*/
        JogadorJogo existingJogadorJogo = pJogadorJogo.read(jogadorjogo.getId());

        if (existingJogadorJogo == null){
            return pJogadorJogo.create(jogadorjogo);
        }else {
            Log.i(TAG, "The player[" + jogadorjogo.getId() + "] already exists");
            return false;
        }

    }

    /*public List<Jogador> getAllJogadores() {

        List<Jogador> jogadores = pJogadores.readAllByNome();

        Log.d(TAG, "Found ["+jogadores.size()+"] players");

        return jogadores;

    }
    public List<Jogador> getAllJogadoresDoJogo(Long idjogo){
        List<Jogador> jogadores= pJogadores.jogadoresDoJogo(idjogo);

        Log.d(TAG, "Found ["+jogadores.size()+"] players of the selected game");

        return jogadores;
    }*/


}

