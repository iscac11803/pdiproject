package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class JogadorTreinoDataParser {

    @SerializedName("data")
    private List<JogadorTreinoParser> jogadorestreino;

    @SerializedName("version")
    private String version;

    @SerializedName("language")
    private String language;


    public void setJogador(List<JogadorTreinoParser> jogadordotreino) {
        this.jogadorestreino = jogadordotreino;
    }

    public List<JogadorTreinoParser> getJogadoresDoTreino() {
        return jogadorestreino;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
