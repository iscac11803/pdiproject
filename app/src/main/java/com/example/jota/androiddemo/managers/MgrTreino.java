package com.example.jota.androiddemo.managers;

import android.util.Log;

import com.example.jota.androiddemo.model.Treino;
import com.example.jota.androiddemo.persistance.PTreino;

import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class MgrTreino {

    private static String TAG = "MgrTreinos";

    private PTreino pTreino;

    public MgrTreino() {
        pTreino = new PTreino();
    }

    public Treino getTreino(String data, String categoria, String hora, String localidade, String campo) {


        Treino treino = pTreino.read(data, categoria, hora, localidade, campo);

        return treino;

    }

    public boolean addTreino(Treino treino) {

        Treino existingTreinos = pTreino.read(treino.getData(), treino.getCategoria(), treino.getHora(), treino.getLocalidade(), treino.getCampo());

        if (existingTreinos == null) {
            return pTreino.create(treino);
        } else {
            Log.i(TAG, "The training[" + treino.getId() + "] already exists");
            return false;
        }
    }

    public List<Treino> getAllTreinos() {

        List<Treino> treinos = pTreino.readAll();

        Log.d(TAG, "Found [" + treinos.size() + "] treinos");

        return treinos;

    }

    public List<Treino> getAllTreinoByDate() {

        List<Treino> treinos = pTreino.readAllByDate();
        Log.d(TAG, "Found [" + treinos.size() + "] trainins");
        return treinos;

    }


    public Long getIdTreino(Treino treino) {
        long idtreino;
        Treino treino2;

        treino2 = pTreino.read(treino.getData(), treino.getCategoria(), treino.getHora(), treino.getLocalidade(), treino.getCampo());
        idtreino = treino2.getId();

        return idtreino;
    }

    public void uptateTreinoById(Treino treino) {
        Treino treino1 = new Treino();
        treino1 = treino;
        pTreino.update(treino1);
    }

    public boolean apagaJogadorDoTreino(Treino treino) {
        if (pTreino.delete(treino)) {
            return true;
        } else {
            return false;
        }

    }

}
