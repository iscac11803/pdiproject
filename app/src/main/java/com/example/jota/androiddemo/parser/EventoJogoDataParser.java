package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jota on 08-06-2015.
 */
public class EventoJogoDataParser {

    @SerializedName("data")
    private List<EventoJogoParser> eventosjogo;

    @SerializedName("version")
    private String version;

    @SerializedName("language")
    private String language;



    public List<EventoJogoParser> getEventosjogo() {
        return eventosjogo;
    }

    public void setJogador(List<EventoJogoParser> eventojogos) {
        this.eventosjogo = eventojogos;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
