package com.example.jota.androiddemo.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.commons.GenericRestClient;
import com.example.jota.androiddemo.model.Jogo;
import com.example.jota.androiddemo.parser.JogoDataParser;
import com.example.jota.androiddemo.parser.JogoParser;
import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 21-05-2015.
 */
public class JogoLoader extends AsyncTask<String, Integer, Boolean> {

    private final String TAG = "JogosLoader";
    private final Context context;

    public JogoLoader(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Fazer qualquer coisa antes de iniciar a
        // tarefa (util para iniciar uma caixa de progresso)
    }

    @Override
    protected Boolean doInBackground(String... params) {
        //Faz coisas em background

        List<Jogo> jogos = new ArrayList();

        String serviceUrl = params[0];

        GenericRestClient rest = new GenericRestClient();
        rest.setLocation(serviceUrl);

        try {
            InputStreamReader result = rest.invoke();
            if (result != null) {
                Gson parser = new Gson();
                JogoDataParser resultParsed = parser.fromJson(result, JogoDataParser.class);
                if (resultParsed != null) {
                    if (resultParsed.getJogos() != null) {
                        for (JogoParser jogoParser : resultParsed.getJogos()) {
                            // idjogo,idtreinador, campo,resultado,eqAdv,campeonato, categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assoFootball,data,observacao,prestacao;
                            Jogo jogo = new Jogo();
                            jogo.setId(jogoParser.getIdjogo());
                            jogo.setTreinadorid(jogoParser.getTreinadorid());
                            jogo.setCampo(jogoParser.getCampo());
                            jogo.setResultado(jogoParser.getResultado());
                            jogo.setEqAdv(jogoParser.getEqAdv());
                            jogo.setCampeonato(jogoParser.getCampeonato());
                            jogo.setCategoria(jogoParser.getCategoria());
                            jogo.setSerie(jogoParser.getSerie());
                            jogo.setLocalidade(jogoParser.getLocalidade());
                            jogo.setHora(jogoParser.getHora());
                            jogo.setJornada(jogoParser.getJornada());
                            jogo.setVisitado(jogoParser.getVisitado());
                            jogo.setVisitante(jogoParser.getVisitante());
                            jogo.setArbitro1(jogoParser.getArbitro1());
                            jogo.setArbitro2(jogoParser.getArbitro2());
                            jogo.setAssoFootball(jogoParser.getAssoFootball());
                            jogo.setData(jogoParser.getData());
                            jogo.setObservacao(jogoParser.getObservacao());
                            jogo.setPrestacao(jogoParser.getPretacao());
                            jogo.setConclusao(jogoParser.getConclusao());
                            String searchValue = jogo.getCampo() + " " +
                                    jogo.getVisitado() + " " +
                                    jogo.getResultado() + " " +
                                    jogo.getVisitante() + " " +
                                    jogo.getCategoria() + " " +
                                    jogo.getData();

                            jogo.setSearchdata(searchValue);
                            System.out.println(jogo.getSearchdata());


                            jogos.add(jogo);
                        }
                    }
                    if (jogos != null && jogos.size() > 0) {
                        for (Jogo jogo : jogos) {

                            boolean addResult = Globals.mgrJogo.addJogo(jogo);
                            if (addResult == false) {
                                Log.e(TAG, "Error while adding game[" + jogo.getId() + "]");
                            }
                        }

                        return true;
                    }
                }
            }

            return false;

        } catch (Exception e) {
            Log.e(TAG, "Error while loading train stations from service [" + Globals.SERVICE_JOGOS_URL + "]", e);
            return null;
        }
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        //Fazer qualquer coisa depois de terminar a tarefa
        // (util para iniciar uma caixa de progresso)
    }

}
