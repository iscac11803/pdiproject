package com.example.jota.androiddemo.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Evento;
import com.example.jota.androiddemo.model.EventoJogo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 08-06-2015.
 */
public class PEventoJogo {
    private static final String TAG = "PEventosJogo";

    private static final String TABLE_NAME = "eventojogo";

    public EventoJogo read(Long ideventojogo) {
        System.out.println("id evento do jogo: " + ideventojogo);
        SQLiteDatabase connection = DBEventoJogo.getConnectionToRead(Globals.context);


        Cursor cursor = connection.
                rawQuery("SELECT * FROM eventojogo WHERE id=?",
                        new String[]{ideventojogo.toString()});

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {


                    Long ideventojogoDb = cursor.getLong(cursor.getColumnIndex("id"));





                    EventoJogo eventojogo = new EventoJogo();
                    eventojogo.setIdeventojogo(ideventojogoDb);



                    return eventojogo;
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

        return null;
    }
    public boolean create(EventoJogo eventojogo) {

        SQLiteDatabase connection = null;

        try {

            connection = DBEventoJogo.getConnectionToWrite(Globals.context);
            ContentValues values = new ContentValues();

            values.put("id", eventojogo.getIdeventojogo());
            values.put("tipodeeventoid", eventojogo.getIdtipoevento());
            values.put("jogadorjogoid", eventojogo.getIdjogadorjogo());
            values.put("tempo", eventojogo.getTempo());


            long insertedId = connection.insert(TABLE_NAME, null, values);

            if (insertedId <= 0) {
                Log.e(PEventoJogo.class.getName(), "Error while inserting the event");
                return false;
            } else {
                eventojogo.setIdeventojogo(insertedId);
            }

            return true;

        }catch (Exception ex){
            Log.e(TAG, "An error while creating event", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
    public List<Evento> eventosDoJogo(Long idjogo){
        SQLiteDatabase connection = DBEventoJogo.getConnectionToRead(Globals.context);
        Cursor cursor = connection.rawQuery("SELECT jogadorjogo.jogadorid, jogadorjogo.jogoid, jogador.nome as nome, " +
                "tipoevento.nome as nomeevento, eventojogo.tempo as tempo FROM " +
                "eventojogo, tipoevento, jogadorjogo, jogador " +
                "WHERE eventojogo.jogadorjogoid = jogadorjogo.jogadorid " +
                "AND jogadorjogo.jogadorid = jogador.id " +
                "AND eventojogo.tipodeeventoid = tipoevento.id " +
                "AND jogadorjogo.jogoid = ?", new String[]{idjogo.toString()});

        List<Evento> eventos = new ArrayList();

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        String eventoDb = cursor.getString(cursor.getColumnIndex("nomeevento"));
                        String tempoeventoDb = cursor.getString(cursor.getColumnIndex("tempo"));
                        String nomejogadoreventoDb=cursor.getString(cursor.getColumnIndex("nome"));

                            Log.e("PEventosJogo", cursor.getString(cursor.getColumnIndex("nome")));

                        Evento evento = new Evento();
                        evento.setNomeevento(eventoDb);
                        evento.setTempo(tempoeventoDb);
                        evento.setJogador(nomejogadoreventoDb);

                        eventos.add(evento);
                        cursor.moveToNext();
                    }
                }
            }

            return eventos;

        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

    }

}

