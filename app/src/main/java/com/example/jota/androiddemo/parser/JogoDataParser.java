package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jota on 21-05-2015.
 */
public class JogoDataParser {

    @SerializedName("data")
    private List<JogoParser> jogos;

    @SerializedName("version")
    private String version;

    @SerializedName("language")
    private String language;


    public List<JogoParser> getJogos() {
        return jogos;
    }

    public void setJogo(List<JogoParser> jogos) {
        this.jogos = jogos;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
