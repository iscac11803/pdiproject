package com.example.jota.androiddemo.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.JogadorTreino;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class PJogadorTreino {
    private static final String TAG = "PJogadoresTreino";
    private static final String CREATE_UTILIZADOR =
            "CREATE TABLE utilizador ( " +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "email TEXT not null, " +
                    "senha TEXT not null " +
                    ");";

    private static final String CREATE_JOGO =
            "CREATE VIRTUAL TABLE jogo USING fts4(id INTEGER PRIMARY KEY ASC,treinadorid,campo,resultado,eqadv,campeonato,categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assofootball,data,observacao,prestacao,conclusao text,searchdata,UNIQUE(id));";
    private static final String CREATE_TREINO = "CREATE VIRTUAL TABLE treino USING fts4(id INTEGER PRIMARY KEY ASC,localidade,categoria,data,campo,hora,searchdata,observacao,UNIQUE(id))";
    private static final String CREATE_JOGADOR = "CREATE VIRTUAL TABLE jogador USING fts4(id INTEGER PRIMARY KEY ASC,nome,datanascimento,nacionalidade,nbicc,nif,nlicenca,categoria,contactomae,nomemae,contactopai,nomepai,dataexamemedico,searchdata,UNIQUE(id))"; //image blob not null
    //private static final String CREATE_JOGADOR_DE_JOGO="CREATE VIRTUAL TABLE jogadorjogo USING fts4(idjogadorjogo,idjogadorfk,idjogofk,idevento,prestacao,observacao,FOREIGN KEY (idjogadorfk) REFERENCES jogador(idjogador),FOREIGN KEY (idjogo) REFERENCES jogo(idjogo),UNIQUE(idjogadorjogo))";
    private static final String CREATE_JOGADOR_DE_JOGO = "CREATE TABLE jogadorjogo(id integer primary key autoincrement,jogadorid integer,jogoid integer,idevento integer,prestacao text,observacao text,FOREIGN KEY(jogadorid) REFERENCES jogador(id),FOREIGN KEY(jogoid) REFERENCES jogo(id))";
    private static final String CREATE_TIPO_DE_EVENTO = "CREATE TABLE tipoevento(id integer primary key autoincrement,nome text)";
    private static final String INSERIR_DADOS_TIPO_EVENTO = "INSERT INTO tipoevento SELECT '1' AS id, 'Cartão Amarelo' AS nome UNION SELECT '2', 'Cartão Vermelho' UNION SELECT '3', 'Golo' UNION SELECT '4', 'Entrada' UNION SELECT '5', 'Saida'";
    private static final String CREATE_EVENTOS_DO_JOGO = "CREATE TABLE eventojogo(id integer primary key autoincrement,tipodeeventoid integer,tempo integer,jogadorjogoid integer,FOREIGN KEY (tipodeeventoid) REFERENCES tipoevento(id),FOREIGN KEY (jogadorjogoid) REFERENCES jogadorjogo(id))";
    private static final String CREATE_JOGADOR_TREINO = "CREATE TABLE jogadortreino(id integer primary key autoincrement,jogadorid integer,treinoid integer,FOREIGN KEY(jogadorid) REFERENCES jogador(id),FOREIGN KEY(treinoid) REFERENCES treino(id))";
    private static final String TABLE_NAME = "jogadortreino";

    public JogadorTreino read(Long idjogadortreino) {

        SQLiteDatabase connection = DBJogadorTreino.getConnectionToRead(Globals.context);


        Cursor cursor = connection.
                rawQuery("SELECT * FROM jogadortreino WHERE rowid=?",
                        new String[]{idjogadortreino.toString()});

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {


                    Long idjogadortreinoDb = cursor.getLong(cursor.getColumnIndex("id"));


                    JogadorTreino jogadortreino = new JogadorTreino();
                    jogadortreino.setId(idjogadortreinoDb);


                    return jogadortreino;
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

        return null;
    }

    public boolean create(JogadorTreino jogadortreino) {

        SQLiteDatabase connection = null;

        try {

            connection = DBJogadorTreino.getConnectionToWrite(Globals.context);
            ContentValues values = new ContentValues();

            values.put("id", jogadortreino.getId());
            values.put("jogadorid", jogadortreino.getJogadorid());
            values.put("treinoid", jogadortreino.getTreinoid());


            long insertedId = connection.insert(TABLE_NAME, null, values);

            if (insertedId <= 0) {
                Log.e(PJogador.class.getName(), "Error while inserting the player");
                return false;
            } else {
                jogadortreino.setJogadorid(insertedId);
            }

            return true;

        } catch (Exception ex) {
            Log.e(TAG, "An error while creating player", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public boolean createJogadoresNoTreino(Long idjogador, Long idtreino) {

        SQLiteDatabase connection = null;

        try {

            connection = DBJogadorTreino.getConnectionToWrite(Globals.context);
            ContentValues values = new ContentValues();


            values.put("jogadorid", idjogador);
            values.put("treinoid", idtreino);


            long insertedId = connection.insert(TABLE_NAME, null, values);

            if (insertedId <= 0) {
                Log.e(PJogador.class.getName(), "Error while inserting the player in the training");
                return false;
            } else {
                System.out.println("Coiso");//jogadortreino.setId(insertedId);
            }

            return true;

        } catch (Exception ex) {
            Log.e(TAG, "An error while creating player", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public boolean update(Long idjogador, Long idtreino) {

        SQLiteDatabase connection = null;

        try {

            connection = DBJogadorTreino.getConnectionToWrite(Globals.context);

            ContentValues values = new ContentValues();

            if (idtreino != null) {
                System.out.println("Jogador com id " + idjogador + " inserido no treino " + idtreino);
                values.put("jogadorid", idjogador);

            }


            long numRowAffected = connection.update(TABLE_NAME, values, "treinoid = ?", new String[]{String.valueOf(idtreino)});

            if (numRowAffected > 0) {
                return true;
            }

            return false;

        } catch (Exception ex) {
            Log.e(TAG, "An error while updating training", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public boolean deleteJogadoresDoTreino(Long idtreino) {
        SQLiteDatabase connection = null;

        try {

            connection = DBJogadorTreino.getConnectionToWrite(Globals.context);

            long numRowsAffected = connection.delete(TABLE_NAME, "treinoid = ?", new String[]{String.valueOf(idtreino)});

            if (numRowsAffected > 0) {
                return true;
            }

            return false;

        } catch (Exception ex) {
            Log.e(TAG, "An error while deleting the players from trainings", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public List<JogadorTreino> readJogadoresDoTreino(Long idtreino) {
        SQLiteDatabase connection = DBJogo.getConnectionToRead(Globals.context);


        Cursor cursor = connection.rawQuery("SELECT rowid as _id,* FROM jogadortreino WHERE treinoid=?", new String[]{String.valueOf(idtreino)});

        List<JogadorTreino> jogadoresDoTreino = new ArrayList();

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        Long idDb = cursor.getLong(cursor.getColumnIndex("_id"));
                        Long idtreinoDb = cursor.getLong(cursor.getColumnIndex("treinoid"));
                        Long idjogadorDb = cursor.getLong(cursor.getColumnIndex("jogadorid"));

                        JogadorTreino jogadortreino = new JogadorTreino();
                        jogadortreino.setId(idDb);
                        jogadortreino.setJogadorid(idjogadorDb);
                        jogadortreino.setTreinoid(idtreinoDb);


                        jogadoresDoTreino.add(jogadortreino);
                        cursor.moveToNext();
                    }
                }
            }

            return jogadoresDoTreino;

        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }
    }

    public boolean atualizaTudo() {
        SQLiteDatabase connection = null;

        try {

            connection = DBJogadorTreino.getConnectionToWrite(Globals.context);

            connection.execSQL("DROP TABLE IF EXISTS utilizador");
            connection.execSQL("DROP TABLE IF EXISTS jogo");
            connection.execSQL("DROP TABLE IF EXISTS jogador");
            connection.execSQL("DROP TABLE IF EXISTS jogadorjogo");
            connection.execSQL("DROP TABLE IF EXISTS tipoevento");
            connection.execSQL("DROP TABLE IF EXISTS treino");
            connection.execSQL("DROP TABLE IF EXISTS jogadortreino");
            connection.execSQL("DROP TABLE IF EXISTS eventojogo");

            connection.execSQL(CREATE_UTILIZADOR);
            connection.execSQL(CREATE_JOGO);
            connection.execSQL(CREATE_JOGADOR);
            connection.execSQL(CREATE_JOGADOR_DE_JOGO);
            connection.execSQL(CREATE_TIPO_DE_EVENTO);
            connection.execSQL(INSERIR_DADOS_TIPO_EVENTO);
            connection.execSQL(CREATE_EVENTOS_DO_JOGO);
            connection.execSQL(CREATE_TREINO);
            connection.execSQL(CREATE_JOGADOR_TREINO);

            return true;


        } catch (Exception ex) {
            Log.e(TAG, "An error while deleting the players from trainings", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

}
