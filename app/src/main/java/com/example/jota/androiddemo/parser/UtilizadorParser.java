package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jota on 18-05-2015.
 */
public class UtilizadorParser {

    @SerializedName("id")
    private Long id;

    @SerializedName("email")
    private String email;

    @SerializedName("senha")
    private String senha;



    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }


}

