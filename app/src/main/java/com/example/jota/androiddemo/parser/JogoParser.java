package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jota on 21-05-2015.
 */
public class JogoParser{

    @SerializedName("id")
    private Long id;

    @SerializedName("treinadorid")
    private Long treinadorid;

    @SerializedName("campo")
    private String campo;

    @SerializedName("resultado")
    private String resultado;

    @SerializedName("eqadv")
    private String eqAdv;

    @SerializedName("campeonato")
    private String campeonato;

    @SerializedName("categoria")
    private String categoria;

    @SerializedName("serie")
    private String serie;

    @SerializedName("localidade")
    private String localidade;

    @SerializedName("hora")
    private String hora;

    @SerializedName("jornada")
    private String jornada;

    @SerializedName("visitado")
    private String visitado;

    @SerializedName("visitante")
    private String visitante;

    @SerializedName("arbitro1")
    private String arbitro1;

    @SerializedName("arbitro2")
    private String arbitro2;

    @SerializedName("assofootball")
    private String assoFootball;

    @SerializedName("data")
    private String data;

    @SerializedName("observacao")
    private String observacao;

    @SerializedName("prestacao")
    private String pretacao;
    /*@SerializedName("searchdata")
    private String searchdata;*/
    @SerializedName("conclusao")
    private String conclusao;



    /*public String getSearchdata() {
        return searchdata;
    }

    public void setSearchdata(String searchdata) {
        this.searchdata = searchdata;
    }*/

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getConclusao() {
        return conclusao;
    }

    public void setConclusao(String conclusao) {
        this.conclusao = conclusao;
    }

    public Long getIdjogo() {
        return id;
    }

    public void setIdjogo(Long idjogo) {
        this.id = idjogo;
    }

    public Long getTreinadorid() {
        return treinadorid;
    }

    public void setTreinadorid(Long idtreinador) {
        this.treinadorid = treinadorid;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getResultado() {
        return resultado;
    }

    public void setResultado(String resultado) {
        this.resultado = resultado;
    }

    public String getEqAdv() {
        return eqAdv;
    }

    public void setEqAdv(String eqAdv) {
        this.eqAdv = eqAdv;
    }

    public String getCampeonato() {
        return campeonato;
    }

    public void setCampeonato(String campeonato) {
        this.campeonato = campeonato;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getSerie() {
        return serie;
    }

    public void setSerie(String serie) {
        this.serie = serie;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getJornada() {
        return jornada;
    }

    public void setJornada(String jornada) {
        this.jornada = jornada;
    }

    public String getVisitado() {
        return visitado;
    }

    public void setVisitado(String visitado) {
        this.visitado = visitado;
    }

    public String getVisitante() {
        return visitante;
    }

    public void setVisitante(String visitante) {
        this.visitante = visitante;
    }

    public String getArbitro1() {
        return arbitro1;
    }

    public void setArbitro1(String arbitro1) {
        this.arbitro1 = arbitro1;
    }

    public String getArbitro2() {
        return arbitro2;
    }

    public void setArbitro2(String arbitro2) {
        this.arbitro2 = arbitro2;
    }

    public String getAssoFootball() {
        return assoFootball;
    }

    public void setAssoFootball(String assoFootball) {
        this.assoFootball = assoFootball;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getPretacao() {
        return pretacao;
    }

    public void setPretacao(String pretacao) {
        this.pretacao = pretacao;
    }
}

