package com.example.jota.androiddemo.persistance;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.jota.androiddemo.app.Globals;

/**
 * Created by Jota on 21-05-2015.
 */
public class DBJogo  extends SQLiteOpenHelper{

    private static final String TAG = "DBJogo";

    private static final int VERSION = 1;
    private static final String DB_NAME = "android_demo.db";
    private static final String CREATE_JOGO =
            "CREATE VIRTUAL TABLE jogo USING fts4( " +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "treinadorid INTEGER not null, " +
                    "campo TEXT not null, " +
                    "resultado TEXT not null, " +
                    "eqadv TEXT not null, " +
                    "campeonato TEXT not null, " +
                    "categoria TEXT not null, " +
                    "serie TEXT not null, " +
                    "localidade TEXT not null, " +
                    "hora TEXT not null, " +
                    "jornada TEXT not null, " +
                    "visitado TEXT not null, " +
                    "visitante TEXT not null, " +
                    "arbitro1 TEXT not null, " +
                    "arbitro2 TEXT not null, " +
                    "assofootball TEXT not null, " +
                    "data TEXT not null, " +
                    "observacao TEXT not null, " +
                    "prestacao TEXT not null " +
                    ");";
//idjogo,idtreinador,campo,resultado,eqAdv,campeonato,escalao,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assoFootball,data,observacao,prestacao

    public DBJogo(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    public static SQLiteDatabase getConnectionToWrite(Context context){

        SQLiteDatabase connection = null;

        DBJogo dbJogo = new DBJogo(Globals.context);

        return dbJogo.getWritableDatabase();

    }

    public static SQLiteDatabase getConnectionToRead(Context context){

        SQLiteDatabase connection = null;

        DBJogo dbJogo = new DBJogo(Globals.context);

        return dbJogo.getReadableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //db.execSQL(CREATE_JOGO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
       /* Log.w(TAG, "Upgrading database from version " + oldVersion +
                " to " + newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS jogo");
        onCreate(db);*/
    }

}
