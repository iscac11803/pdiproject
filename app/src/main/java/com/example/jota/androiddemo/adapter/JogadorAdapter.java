package com.example.jota.androiddemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jota.androiddemo.R;
import com.example.jota.androiddemo.model.Jogador;

import java.util.List;

/**
 * Created by Jota on 19-06-2015.
 */
public class JogadorAdapter extends BaseAdapter {

    private Context context;
    private List<Jogador> jogadores;

    public JogadorAdapter(Context context, List<Jogador> jogadores) {
        super();
        this.context = context;
        this.jogadores = jogadores;
    }

    @Override
    public int getCount() {
        return jogadores.size();
    }

    @Override
    public Object getItem(int position) {
        return jogadores.get(position);
    }

    @Override
    public long getItemId(int position) {
        return jogadores.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.linha_da_lista_de_jogadores, parent, false);
        }

        TextView elementNome = (TextView) convertView.findViewById(R.id.IdJogadorNome);

        Jogador jogador = jogadores.get(position);
        elementNome.setText(jogador.getNome());


        return convertView;
    }
}

