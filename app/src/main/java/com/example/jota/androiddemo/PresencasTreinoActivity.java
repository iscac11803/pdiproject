package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Jogador;

import java.util.ArrayList;
import java.util.List;


public class PresencasTreinoActivity extends Activity {
    MyCustomAdapter dataAdapter = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_presencas_treino);
        Globals.context = getApplicationContext();

        Bundle infopassada = getIntent().getExtras();
        Long idtreino = infopassada.getLong("idtreino");
        Long idutilizador = infopassada.getLong("idutilizador");
        String categoria = infopassada.getString("categoria");
        String campo = infopassada.getString("campo");
        String localidade = infopassada.getString("localidade");
        String data = infopassada.getString("data");
        String hora = infopassada.getString("hora");

        displayListView(categoria);
        checkButtonClick(idtreino, categoria, campo, localidade, data, hora, idutilizador);
    }

    private void displayListView(String categoria) {
        List<Jogador> jogadores = Globals.mgrJogador.getAllJogadoresByCategoria(categoria);


        //create an ArrayAdaptar from the String Array
        dataAdapter = new MyCustomAdapter(this, R.layout.linha_checked_jogador_do_treino, jogadores);
        ListView listView = (ListView) findViewById(R.id.listView1);
        // Assign adapter to ListView
        listView.setAdapter(dataAdapter);


        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                // When clicked, show a toast with the TextView text
                Jogador jogador = (Jogador) parent.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(),
                        "Clicked on Row: " + jogador.getNome(),
                        Toast.LENGTH_LONG).show();

            }
        });

    }

    private void checkButtonClick(final Long idtreino, final String categoria, final String campo, final String localidade, final String data, final String hora, final Long idutilizador) {


        Button myButton = (Button) findViewById(R.id.btnRegPresencas);
        myButton.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {


                List<Jogador> jogadorList = dataAdapter.jogadorList;
                for (int i = 0; i < jogadorList.size(); i++) {
                    Jogador jogador = jogadorList.get(i);
                    if (jogador.isSelected()) {

                        Globals.mgrJogadorTreino.addJogadorTreinoPresenca(jogador.getId(), idtreino);
                        Intent intMenu = new Intent(Globals.context, FinalizaTreinoActivity.class);
                        Bundle dataBundle = new Bundle();
                        dataBundle.putLong("idutilizador", idutilizador);
                        dataBundle.putLong("idtreino", idtreino);
                        dataBundle.putString("categoria", categoria);
                        dataBundle.putString("localidade", localidade);
                        dataBundle.putString("campo", campo);
                        dataBundle.putString("data", data);
                        dataBundle.putString("hora", hora);
                        intMenu.putExtras(dataBundle);

                        startActivity(intMenu);
                        finish();
                    }
                }


            }
        });

    }

    public void onBackPressed() {
        // do nothing.
    }

    private class MyCustomAdapter extends ArrayAdapter<Jogador> {

        private ArrayList<Jogador> jogadorList;

        public MyCustomAdapter(Context context, int textViewResourceId,
                               List<Jogador> jogadorList) {
            super(context, textViewResourceId, jogadorList);
            this.jogadorList = new ArrayList<Jogador>();
            this.jogadorList.addAll(jogadorList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            ViewHolder holder = null;
            Log.v("ConvertView", String.valueOf(position));

            if (convertView == null) {
                LayoutInflater vi = (LayoutInflater) getSystemService(
                        Context.LAYOUT_INFLATER_SERVICE);
                convertView = vi.inflate(R.layout.linha_checked_jogador_do_treino, null);

                holder = new ViewHolder();
                //holder.code = (TextView) convertView.findViewById(R.id.code);
                holder.name = (CheckBox) convertView.findViewById(R.id.checkBox1);
                convertView.setTag(holder);

                holder.name.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        Jogador jogador = (Jogador) cb.getTag();
                        jogador.setSelected(cb.isChecked());
                    }
                });
            } else {
                holder = (ViewHolder) convertView.getTag();
            }

            Jogador jogador = jogadorList.get(position);
            //holder.code.setText(" (" + jogador.getCode() + ")");
            holder.name.setText(jogador.getNome());
            holder.name.setChecked(jogador.isSelected());
            holder.name.setTag(jogador);

            return convertView;

        }

        private class ViewHolder {
            TextView code;
            CheckBox name;
        }
    }

}


