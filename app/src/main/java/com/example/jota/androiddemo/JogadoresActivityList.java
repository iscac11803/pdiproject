package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jota.androiddemo.adapter.JogadorAdapter;
import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Jogador;

import java.util.List;


public class JogadoresActivityList extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.jogadores_activity_list);

        Globals.context = getApplicationContext();

        ListView listJogadores = (ListView) findViewById(R.id.IdListaJogador);

        List<Jogador> jogadores = Globals.mgrJogador.getJogadoresByName();
        JogadorAdapter adapter = new JogadorAdapter(Globals.context, jogadores);
        listJogadores.setAdapter(adapter);

        Button botaoPesqAvan = (Button) findViewById(R.id.btnjogospesqavanjogadores);
        botaoPesqAvan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intMenu = new Intent(Globals.context, SearchViewJogadoresActivity.class);
                startActivity(intMenu);
            }
        });

        listJogadores.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {


                //idjogo,idtreinador,campo,resultado,eqAdv,campeonato,categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assofootball,data,observacao,prestacao

                TextView tvnomejogador = ((TextView) view.findViewById(R.id.IdJogadorNome));


                String nomejogador = tvnomejogador.getText().toString();

                Jogador jogador = Globals.mgrJogador.getJogador(nomejogador);

                Bundle dataBundle = new Bundle();
                dataBundle.putLong("id", jogador.getId());
                dataBundle.putString("nome", jogador.getNome());
                dataBundle.putString("datanascimento", jogador.getDatanascimento());
                dataBundle.putString("nbicc", jogador.getNbicc());
                dataBundle.putString("nif", jogador.getNif());
                dataBundle.putString("nlicenca", jogador.getNlicenca());
                dataBundle.putString("categoria", jogador.getCategoria());
                dataBundle.putString("contactomae", jogador.getContactomae());
                dataBundle.putString("nomemae", jogador.getNomemae());
                dataBundle.putString("contactopai", jogador.getContactopai());
                dataBundle.putString("nomepai", jogador.getNomepai());
                dataBundle.putString("dataexamemedico", jogador.getDataexame());

                System.out.println("dados do click : " + jogador.toString());

                Intent intMain = new Intent(Globals.context, DadosDoJogadorActivity.class);
                intMain.putExtras(dataBundle);
                startActivity(intMain);
                finish();


            }
        });

    }


    }



