package com.example.jota.androiddemo.persistance;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Jota on 25-06-2015.
 */
public class TreinoDbAdapter {


    public static final String CATEGORIA = "categoria";
    public static final String LOCALIDADE = "localidade";
    public static final String HORA = "hora";
    public static final String DATA = "data";
    public static final String KEY_SEARCH = "keysearch";

    private static final String TAG = "CustomersDbAdapter";
    private static final String DATABASE_NAME = "android_demo.db";
    private static final String FTS_VIRTUAL_TABLE = "treino";
    private static final int DATABASE_VERSION = 1;
    //Create a FTS3 Virtual Table for fast searches

    private final Context mCtx;
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    public TreinoDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public TreinoDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }


    public Cursor searchTreino(String inputText) throws SQLException {
        Log.w(TAG, inputText);

        String query = "SELECT rowid as _id,* from treino where categoria MATCH '" + inputText + "'";


        Log.w(TAG, query);
        Cursor mCursor = mDb.rawQuery(query, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }


    public boolean deleteAllJogos() {

        int doneDelete = 0;
        doneDelete = mDb.delete(FTS_VIRTUAL_TABLE, null, null);
        Log.w(TAG, Integer.toString(doneDelete));
        return doneDelete > 0;

    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            /*Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);*/
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            /*Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + FTS_VIRTUAL_TABLE);
            onCreate(db);*/
        }
    }
}


