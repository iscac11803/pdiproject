package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jota on 04-06-2015.
 */
public class JogadorJogoParser {
    @SerializedName("id")
    private Long id;

    @SerializedName("jogadorid")
    private Long jogadorid;

    @SerializedName("jogoid")
    private Long jogoid;

    @SerializedName("idevento")
    private Long idevento;

    @SerializedName("observacao")
    private String observacao;

    @SerializedName("prestacao")
    private String prestacao;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJogadorid() {
        return jogadorid;
    }

    public void setJogadorid(Long jogadorid) {
        this.jogadorid = jogadorid;
    }

    public Long getJogoid() {
        return jogoid;
    }

    public void setJogoid(Long jogoid) {
        this.jogoid = jogoid;
    }

    public Long getIdevento() {
        return idevento;
    }

    public void setIdevento(Long idevento) {
        this.idevento = idevento;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public String getPrestacao() {
        return prestacao;
    }

    public void setPrestacao(String prestacao) {
        this.prestacao = prestacao;
    }
}
