package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;

import com.example.jota.androiddemo.persistance.JogoDbAdapter;

public class SearchViewJogosActivity extends Activity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    public static Context context;
    private ListView mListView;
    private SearchView searchView;
    private JogoDbAdapter mDbHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        context=getApplicationContext();
        searchView = (SearchView) findViewById(R.id.search);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);

        mListView = (ListView) findViewById(R.id.list);
        /*inspectionDate = (TextView) findViewById(R.id.inspectionDate);
        displayDate();*/

        mDbHelper = new JogoDbAdapter(this);
        mDbHelper.open();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDbHelper  != null) {
            mDbHelper.close();
        }
    }

    public boolean onQueryTextChange(String newText) {
        showResults(newText + "*");
        return false;
    }

    public boolean onQueryTextSubmit(String query) {
        showResults(query + "*");
        return false;
    }

    public boolean onClose() {
        showResults("");
        return false;
    }

    private void showResults(String query) {

        Cursor cursor = mDbHelper.searchJogo((query != null ? query.toString() : "@@@@"));

        if (cursor == null) {
            //
        } else {
            // Specify the columns we want to display in the result
            String[] from = new String[] {

                    JogoDbAdapter.CAMPO,
                    JogoDbAdapter.RESULTADO,
                    JogoDbAdapter.CATEGORIA,
                    JogoDbAdapter.DATA,
                    JogoDbAdapter.VISITADO,
                    JogoDbAdapter.VISITANTE};


            // Specify the Corresponding layout elements where we want the columns to go
            int[] to = new int[] {
                    R.id.txtIdCampo,
                    R.id.txtIdJogoResultado,
                    R.id.txtIdJogoCategoria,
                    R.id.txtIdJogoData,
                    R.id.txtIdJogoVisitado,
                    R.id.txtIdJogoVisitante};

            // Create a simple cursor adapter for the definitions and apply them to the ListView
            SimpleCursorAdapter customers = new SimpleCursorAdapter(this,R.layout.customerresult, cursor, from, to);
            mListView.setAdapter(customers);

            // Define the on-click listener for the list items
            mListView.setOnItemClickListener(new OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    Cursor cursor = (Cursor) mListView.getItemAtPosition(position);
                    String campo  = cursor.getString(cursor.getColumnIndexOrThrow("campo"));
                    String resultado = cursor.getString(cursor.getColumnIndexOrThrow("resultado"));
                    String eqadv = cursor.getString(cursor.getColumnIndexOrThrow("eqadv"));
                    String campeonato = cursor.getString(cursor.getColumnIndexOrThrow("campeonato"));
                    String categoria = cursor.getString(cursor.getColumnIndexOrThrow("categoria"));
                    String serie = cursor.getString(cursor.getColumnIndexOrThrow("serie"));
                    String localidade = cursor.getString(cursor.getColumnIndexOrThrow("localidade"));
                    String hora = cursor.getString(cursor.getColumnIndexOrThrow("hora"));
                    String jornada = cursor.getString(cursor.getColumnIndexOrThrow("jornada"));
                    String visitado = cursor.getString(cursor.getColumnIndexOrThrow("visitado"));
                    String visitante = cursor.getString(cursor.getColumnIndexOrThrow("visitante"));
                    String arbitro1 = cursor.getString(cursor.getColumnIndexOrThrow("arbitro1"));
                    String arbitro2 = cursor.getString(cursor.getColumnIndexOrThrow("arbitro2"));
                    String assofootball = cursor.getString(cursor.getColumnIndexOrThrow("assofootball"));
                    String data = cursor.getString(cursor.getColumnIndexOrThrow("data"));
                    String observacao = cursor.getString(cursor.getColumnIndexOrThrow("observacao"));
                    String prestacao = cursor.getString(cursor.getColumnIndexOrThrow("prestacao"));
                    String conclusao = cursor.getString(cursor.getColumnIndex("conclusao"));
                    Long idjogo =cursor.getLong(cursor.getColumnIndex("_id"));
                    //Long idutilizador =cursor.getLong(cursor.getColumnIndex("idutilizador"));
                    System.out.println("id do jogo"+idjogo);
                    //campo,resultado,eqAdv,campeonato, categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assoFootball,data,observacao,prestacao

                    Bundle dataBundle = new Bundle();

                    dataBundle.putString("campo",campo);
                    dataBundle.putString("resultado",resultado);
                    dataBundle.putString("visitado",visitado);
                    dataBundle.putString("visitante",visitante);
                    dataBundle.putString("data",data);
                    dataBundle.putString("categoria",categoria);
                    dataBundle.putString("eqadv",eqadv);
                    dataBundle.putString("campeonato",campeonato);
                    dataBundle.putString("serie",serie);
                    dataBundle.putString("localidade",localidade);
                    dataBundle.putString("hora",hora);
                    dataBundle.putString("jornada",jornada);
                    dataBundle.putString("arbitro1",arbitro1);
                    dataBundle.putString("arbitro2",arbitro2);
                    dataBundle.putString("assofootball",assofootball);
                    dataBundle.putString("observacao",observacao);
                    dataBundle.putString("prestacao",prestacao);
                    dataBundle.putString("conclusao", conclusao);
                    dataBundle.putLong("idjogo",idjogo);
                    Intent intMain = new Intent(context,DadosJogoActivity.class);
                    intMain.putExtras(dataBundle);
                    startActivity(intMain);
                    finish();

                }
            });
        }
    }


}