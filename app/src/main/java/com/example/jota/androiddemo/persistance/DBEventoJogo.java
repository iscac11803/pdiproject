package com.example.jota.androiddemo.persistance;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.jota.androiddemo.app.Globals;

/**
 * Created by Jota on 08-06-2015.
 */
public class DBEventoJogo extends SQLiteOpenHelper {

    private static final String TAG = "DBEventoJogo";

    private static final int VERSION = 1;
    private static final String DB_NAME = "android_demo.db";


    public DBEventoJogo(Context context) {
        super(context, DB_NAME, null, VERSION);
    }

    public static SQLiteDatabase getConnectionToWrite(Context context){

        SQLiteDatabase connection = null;

        DBEventoJogo dbEventoJogo = new DBEventoJogo(Globals.context);

        return dbEventoJogo.getWritableDatabase();

    }

    public static SQLiteDatabase getConnectionToRead(Context context){

        SQLiteDatabase connection = null;

        DBEventoJogo dbEventoJogo = new DBEventoJogo(Globals.context);

        return dbEventoJogo.getReadableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

}
