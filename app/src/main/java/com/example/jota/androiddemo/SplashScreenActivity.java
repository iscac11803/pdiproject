package com.example.jota.androiddemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.Window;
import android.view.WindowManager;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.persistance.PUser;
import com.example.jota.androiddemo.tasks.EventoDoJogoLoader;
import com.example.jota.androiddemo.tasks.JogadorDoJogoLoader;
import com.example.jota.androiddemo.tasks.JogadorDoTreinoLoader;
import com.example.jota.androiddemo.tasks.JogadorLoader;
import com.example.jota.androiddemo.tasks.JogoLoader;
import com.example.jota.androiddemo.tasks.TreinoLoader;
import com.example.jota.androiddemo.tasks.UserAccountLoader;


public class SplashScreenActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        Globals.context = getApplicationContext();
        setContentView(R.layout.activity_splash_screen);
        Globals.exameMedico = true;
        Handler handler = new Handler();

        if(Globals.firstTime=false) {
            PUser pUser = new PUser();
            pUser.primeiraVez();
            System.out.println("Correu o firstime");

        }
        handler.postDelayed(new Runnable() {
            @Override

            public void run() {

                if (isNetworkAvailable())
                {

                    UserAccountLoader userAccountLoader = new UserAccountLoader(Globals.context);
                    userAccountLoader.execute(Globals.SERVICE_USERS_URL);


                    JogoLoader jogoLoader = new JogoLoader(Globals.context);
                    jogoLoader.execute(Globals.SERVICE_JOGOS_URL);
                    //é iniciada uma thread com um rest service que armazema os dados contidos no url do pastebin


                    JogadorLoader jogadorLoader = new JogadorLoader(Globals.context);
                    jogadorLoader.execute(Globals.SERVICE_JOGADORES_URL);

                    JogadorDoJogoLoader jogadoresJogoLoader = new JogadorDoJogoLoader(Globals.context);
                    jogadoresJogoLoader.execute(Globals.SERVICE_JOGADORES_DOS_JOGOS);

                    EventoDoJogoLoader eventosJogoLoader = new EventoDoJogoLoader(Globals.context);
                    eventosJogoLoader.execute(Globals.SERVICE_EVENTOS_DO_JOGO);

                    TreinoLoader treinoLoader = new TreinoLoader(Globals.context);
                    treinoLoader.execute(Globals.SERVICE_TREINOS);

                    JogadorDoTreinoLoader jogadorDoTreinoLoader = new JogadorDoTreinoLoader(Globals.context);
                    jogadorDoTreinoLoader.execute(Globals.SERVICE_JOGADORES_TREINO);

                    Intent intMain = new Intent(Globals.context, LoginActivity.class);
                    startActivity(intMain);
                    finish();
                }
                else
                {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(SplashScreenActivity.this);

                    // Setting Dialog Title
                    alertDialog.setTitle("Erro de ligação");

                    // Setting Dialog Message
                    alertDialog.setMessage("Acesso à internet não detetado, deseja prosseguir com base de dados desatualizada?");

                    // Setting Icon to Dialog
                    alertDialog.setIcon(R.drawable.wifi);

                    // Setting Positive "Yes" Button
                    alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intMain = new Intent(Globals.context, LoginActivity.class);
                            startActivity(intMain);

                        }
                    });

                    // Setting Negative "NO" Button
                    alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            finish();
                            dialog.cancel();
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();



                }
            }
        }, 5000);

    }


    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    @Override
    protected void onResume() {
        super.onResume();


        Globals.context = getApplicationContext();


    }

}
