package com.example.jota.androiddemo.model;

/**
 * Created by Jota on 24-06-2015.
 */
public class Treino {

    private String localidade;
    private String categoria;
    private String data;
    private String campo;
    private String hora;
    private String searchdata;
    private String observacao;
    private Long id;

    public Treino() {

    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getLocalidade() {
        return localidade;
    }

    public void setLocalidade(String localidade) {
        this.localidade = localidade;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getCampo() {
        return campo;
    }

    public void setCampo(String campo) {
        this.campo = campo;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getSearchdata() {
        return searchdata;
    }

    public void setSearchdata(String searchdata) {
        this.searchdata = searchdata;
    }


    @Override
    public String toString() {
        return "Treino{" +
                "localidade='" + localidade + '\'' +
                ", categoria='" + categoria + '\'' +
                ", data='" + data + '\'' +
                ", campo='" + campo + '\'' +
                ", hora='" + hora + '\'' +
                ", searchdata='" + searchdata + '\'' +
                ", observacao='" + observacao + '\'' +
                ", id=" + id +
                '}';
    }
}
