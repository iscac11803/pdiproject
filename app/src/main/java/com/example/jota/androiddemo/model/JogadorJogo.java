package com.example.jota.androiddemo.model;

/**
 * Created by Jota on 04-06-2015.
 */
public class JogadorJogo {
    private Long id, jogoid, jogadorid, idevento;
    private String prestacao,observacao;

    public JogadorJogo() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getJogoid() {
        return jogoid;
    }

    public void setJogoid(Long jogoid) {
        this.jogoid = jogoid;
    }

    public Long getJogadorid() {
        return jogadorid;
    }

    public void setJogadorid(Long jogadorid) {
        this.jogadorid = jogadorid;
    }

    public Long getIdevento() {
        return idevento;
    }

    public void setIdevento(Long idevento) {
        this.idevento = idevento;
    }

    public String getPrestacao() {
        return prestacao;
    }

    public void setPrestacao(String prestacao) {
        this.prestacao = prestacao;
    }

    public String getObservacao() {
        return observacao;
    }

    public void setObservacao(String observacao) {
        this.observacao = observacao;
    }
}
