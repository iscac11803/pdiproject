package com.example.jota.androiddemo.parser;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jota on 08-06-2015.
 */
public class EventoJogoParser {

    @SerializedName("id")
    private Long id;

    @SerializedName("tipodeeventoid")
    private Long tipodeeventoid;

    @SerializedName("jogadorjogoid")
    private Long jogadorjogoid;

    @SerializedName("tempo")
    private Long tempo;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getTipodeeventoid() {
        return tipodeeventoid;
    }

    public void setTipodeeventoid(Long tipodeeventoid) {
        this.tipodeeventoid = tipodeeventoid;
    }

    public Long getJogadorjogoid() {
        return jogadorjogoid;
    }

    public void setJogadorjogoid(Long idjogofk) {
        this.jogadorjogoid = jogadorjogoid;
    }

    public Long getTempo() {
        return tempo;
    }

    public void setTempo(Long tempo) {
        this.tempo = tempo;
    }
}
