package com.example.jota.androiddemo.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.commons.GenericRestClient;
import com.example.jota.androiddemo.model.Treino;
import com.example.jota.androiddemo.parser.TreinoDataParser;
import com.example.jota.androiddemo.parser.TreinoParser;
import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class TreinoLoader extends AsyncTask<String, Integer, Boolean> {

    private final String TAG = "TreinosLoader";
    private final Context context;

    public TreinoLoader(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Fazer qualquer coisa antes de iniciar a
        // tarefa (util para iniciar uma caixa de progresso)
    }

    @Override
    protected Boolean doInBackground(String... params) {
        //Faz coisas em background

        List<Treino> treinos = new ArrayList();

        String serviceUrl = params[0];

        GenericRestClient rest = new GenericRestClient();
        rest.setLocation(serviceUrl);

        try {
            InputStreamReader result = rest.invoke();
            if (result != null) {
                Gson parser = new Gson();
                TreinoDataParser resultParsed = parser.fromJson(result, TreinoDataParser.class);
                if (resultParsed != null) {
                    if (resultParsed.getTreinos() != null) {
                        for (TreinoParser treinoParser : resultParsed.getTreinos()) {
                            Treino treino = new Treino();
                            treino.setId(treinoParser.getId());
                            treino.setCampo(treinoParser.getCampo());
                            treino.setCategoria(treinoParser.getCategoria());
                            treino.setLocalidade(treinoParser.getLocalidade());
                            treino.setHora(treinoParser.getHora());
                            treino.setData(treinoParser.getData());
                            treino.setObservacao(treinoParser.getObservacao());

                            treinos.add(treino);
                        }
                    }
                    if (treinos != null && treinos.size() > 0) {
                        for (Treino treino : treinos) {

                            boolean addResult = Globals.mgrTreino.addTreino(treino);
                            if (addResult == false) {
                                Log.e(TAG, "Error while adding training[" + treino.getId() + "]");
                            }
                        }

                        return true;
                    }
                }
            }

            return false;

        } catch (Exception e) {
            Log.e(TAG, "Error while loading train stations from service [" + Globals.SERVICE_TREINOS + "]", e);
            return null;
        }
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        //Fazer qualquer coisa depois de terminar a tarefa
        // (util para iniciar uma caixa de progresso)
    }

}

