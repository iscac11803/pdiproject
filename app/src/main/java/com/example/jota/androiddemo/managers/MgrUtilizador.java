package com.example.jota.androiddemo.managers;

import android.util.Log;

import com.example.jota.androiddemo.model.Utilizador;
import com.example.jota.androiddemo.persistance.PUser;

import java.util.List;
//responsavel por gerir os dados de todos os utilizadores


public class MgrUtilizador {

    private static String TAG = "MgrUsers";

    private PUser pUser;

    public MgrUtilizador() {
        pUser = new PUser();
    }

    public Utilizador getUser(String email, String password) {

        //1-Seria respons�vel por garantir que quem pede o objecto tem permiss�es para tal
        //2-� respons�vel de garantir o envio completo do objecto


        Utilizador utilizador = pUser.read(email, password);

        return utilizador;

    }

    public boolean addUser(Utilizador utilizador) {

        Utilizador existingUtilizador = pUser.read(utilizador.getEmail(), utilizador.getSenha());


        if (existingUtilizador == null) {
            return pUser.create(utilizador);
        } else {
            Log.i(TAG, "The user[" + utilizador.getEmail() + "] already exists");
            return false;
        }

    }

    public List<Utilizador> getAllUsers() {

        List<Utilizador> utilizadors = pUser.readAll();

        Log.d(TAG, "Found [" + utilizadors.size() + "] users");

        return utilizadors;

    }


}
