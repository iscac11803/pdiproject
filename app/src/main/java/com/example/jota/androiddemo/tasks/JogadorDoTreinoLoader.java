package com.example.jota.androiddemo.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.commons.GenericRestClient;
import com.example.jota.androiddemo.model.JogadorTreino;
import com.example.jota.androiddemo.parser.JogadorTreinoDataParser;
import com.example.jota.androiddemo.parser.JogadorTreinoParser;
import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class JogadorDoTreinoLoader extends AsyncTask<String, Integer, Boolean> {

    private final String TAG = "JogadoresDoTreinoLoader";
    private final Context context;


    public JogadorDoTreinoLoader(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... params) {
        //Faz coisas em background
        List<JogadorTreino> jogadoresdotreino = new ArrayList();

        String serviceUrl = params[0];

        GenericRestClient rest = new GenericRestClient();
        rest.setLocation(serviceUrl);

        try {
            InputStreamReader result = rest.invoke();
            if (result != null) {
                Gson parser = new Gson();
                JogadorTreinoDataParser resultParsed = parser.fromJson(result, JogadorTreinoDataParser.class);
                if (resultParsed != null) {
                    if (resultParsed.getJogadoresDoTreino() != null) {
                        for (JogadorTreinoParser jogadortreinoParser : resultParsed.getJogadoresDoTreino()) {
                            JogadorTreino jogadortreino = new JogadorTreino();
                            jogadortreino.setId(jogadortreinoParser.getId());
                            jogadortreino.setJogadorid(jogadortreinoParser.getJogadorid());
                            jogadortreino.setTreinoid(jogadortreinoParser.getTreinoid());

                            jogadoresdotreino.add(jogadortreino);
                        }
                    }
                    if (jogadoresdotreino != null && jogadoresdotreino.size() > 0) {
                        for (JogadorTreino jogadorTreino : jogadoresdotreino) {

                            boolean addResult = Globals.mgrJogadorTreino.addJogadorTreino(jogadorTreino);
                            if (addResult == false) {
                                Log.e(TAG, "Error while adding player[" + jogadorTreino.getJogadorid() + "]");
                            }
                        }

                        return true;
                    }
                }
            }

            return false;

        } catch (Exception e) {
            Log.e(TAG, "Error while loading train stations from service [" + Globals.SERVICE_JOGADORES_TREINO + "]", e);
            return null;
        }
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        //Fazer qualquer coisa depois de terminar a tarefa
        // (util para iniciar uma caixa de progresso)
    }

}

