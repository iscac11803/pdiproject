package com.example.jota.androiddemo.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Jogo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 20-05-2015.
 */
public class PJogo {

    private static final String TAG = "PJogos";

    private static final String TABLE_NAME = "jogo";

    //idjogo,idtreinador,campo,resultado,eqAdv,campeonato,escalao,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assoFootball,data,observacao,prestacao
    public PJogo() {
    }

    public boolean create(Jogo jogo) {

        SQLiteDatabase connection = null;

        try {

            connection = DBJogo.getConnectionToWrite(Globals.context);
            ContentValues values = new ContentValues();

            values.put("id", jogo.getId());
            values.put("treinadorid", jogo.getTreinadorid());
            values.put("campo", jogo.getCampo());
            values.put("resultado", jogo.getResultado());
            values.put("eqadv", jogo.getEqAdv());
            values.put("campeonato", jogo.getCampeonato());
            values.put("categoria",jogo.getCategoria());
            values.put("serie",jogo.getSerie());
            values.put("localidade", jogo.getLocalidade());
            values.put("hora", jogo.getHora());
            values.put("jornada", jogo.getJornada());
            values.put("visitado", jogo.getVisitado());
            values.put("visitante", jogo.getVisitante());
            values.put("arbitro1", jogo.getArbitro1());
            values.put("arbitro2", jogo.getArbitro2());
            values.put("assofootball", jogo.getAssoFootball());
            values.put("data", jogo.getData());
            values.put("observacao", jogo.getObservacao());
            values.put("prestacao", jogo.getPrestacao());
            values.put("conclusao", jogo.getConclusao());
            values.put("searchdata",jogo.getSearchdata());


            long insertedId = connection.insert(TABLE_NAME, null, values);

            if (insertedId <= 0) {
                Log.e(PJogo.class.getName(), "Error while inserting the game");
                return false;
            } else {
                jogo.setId(insertedId);
            }

            return true;

        }catch (Exception ex){
            Log.e(TAG, "An error while creating game", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    public Jogo read(String data, String categoria, String visitado, String visitante, String resultado, String campo) {

        SQLiteDatabase connection = DBJogo.getConnectionToRead(Globals.context);


        Cursor cursor = connection.rawQuery("SELECT rowid as _id,* FROM jogo WHERE data = ? AND categoria=? AND visitado=? AND visitante=? AND resultado=? AND campo=?",
                        new String[]{data, categoria, visitado, visitante, resultado, campo});

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {


                    //idjogo,idtreinador,campo,resultado,eqAdv,campeonato,categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assofootball,data,observacao,prestacao

                    Long idjogoDb = cursor.getLong(cursor.getColumnIndexOrThrow("_id"));
                    String campoDb = cursor.getString(cursor.getColumnIndexOrThrow("campo"));
                    String resultadoDb = cursor.getString(cursor.getColumnIndexOrThrow("resultado"));
                    String edAdvDb = cursor.getString(cursor.getColumnIndexOrThrow("eqadv"));
                    String campeonadoDb = cursor.getString(cursor.getColumnIndexOrThrow("campeonato"));
                    String categoriaDb = cursor.getString(cursor.getColumnIndexOrThrow("categoria"));
                    String serieDb = cursor.getString(cursor.getColumnIndexOrThrow("serie"));
                    String localidadeDb = cursor.getString(cursor.getColumnIndexOrThrow("localidade"));
                    String horaDb = cursor.getString(cursor.getColumnIndexOrThrow("hora"));
                    String jornadaDb = cursor.getString(cursor.getColumnIndexOrThrow("jornada"));
                    String visitadoDb = cursor.getString(cursor.getColumnIndexOrThrow("visitado"));
                    String visitanteDb = cursor.getString(cursor.getColumnIndexOrThrow("visitante"));
                    String arbitro1Db = cursor.getString(cursor.getColumnIndexOrThrow("arbitro1"));
                    String arbitro2Db = cursor.getString(cursor.getColumnIndexOrThrow("arbitro2"));
                    String assoFootballDb = cursor.getString(cursor.getColumnIndexOrThrow("assofootball"));
                    String dataDb = cursor.getString(cursor.getColumnIndexOrThrow("data"));
                    String observacaoDb = cursor.getString(cursor.getColumnIndexOrThrow("observacao"));
                    String prestacaoDb = cursor.getString(cursor.getColumnIndexOrThrow("prestacao"));
                    String conclusaoDb = cursor.getString(cursor.getColumnIndexOrThrow("conclusao"));


                    Jogo jogo = new Jogo();
                    jogo.setId(idjogoDb);
                    // jogo.setTreinadorid(idtreinadorDb);
                    jogo.setCampo(campoDb);
                    jogo.setResultado(resultadoDb);
                    jogo.setEqAdv(edAdvDb);
                    jogo.setCampeonato(campeonadoDb);
                    jogo.setCategoria(categoriaDb);
                    jogo.setSerie(serieDb);
                    jogo.setLocalidade(localidadeDb);
                    jogo.setHora(horaDb);
                    jogo.setJornada(jornadaDb);
                    jogo.setVisitado(visitadoDb);
                    jogo.setVisitante(visitanteDb);
                    jogo.setArbitro1(arbitro1Db);
                    jogo.setArbitro2(arbitro2Db);
                    jogo.setAssoFootball(assoFootballDb);
                    jogo.setData(dataDb);
                    jogo.setObservacao(observacaoDb);
                    jogo.setPrestacao(prestacaoDb);
                    jogo.setConclusao(conclusaoDb);

                    return jogo;
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

        return null;
    }

    public boolean update(Jogo jogo) {

        SQLiteDatabase connection = null;

        try {

            connection = DBJogo.getConnectionToWrite(Globals.context);

            ContentValues values = new ContentValues();

            if (jogo.getId() != null) {
                values.put("idjogo", jogo.getId());
                values.put("idtreinador", jogo.getTreinadorid());
                values.put("campo", jogo.getCampo());
                values.put("resultado", jogo.getResultado());
                values.put("eqadv", jogo.getEqAdv());
                values.put("campeonato", jogo.getCampeonato());
                values.put("categoria",jogo.getCategoria());
                values.put("serie",jogo.getSerie());
                values.put("localidade", jogo.getLocalidade());
                values.put("hora", jogo.getHora());
                values.put("jornada", jogo.getJornada());
                values.put("visitado", jogo.getVisitado());
                values.put("visitante", jogo.getVisitante());
                values.put("arbitro1", jogo.getArbitro1());
                values.put("arbitro2", jogo.getArbitro2());
                values.put("assofootball", jogo.getAssoFootball());
                values.put("data", jogo.getData());
                values.put("observacao", jogo.getObservacao());
                values.put("prestacao", jogo.getPrestacao());
            }


            long numRowAffected = connection.update(TABLE_NAME, values, "id = ?", new String[]{String.valueOf(jogo.getId())});

            if (numRowAffected > 0) {
                return true;
            }

            return false;

        }catch (Exception ex){
            Log.e(TAG, "An error while updating game", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public boolean delete(Jogo jogo) {
        SQLiteDatabase connection = null;

        try {

            connection = DBJogo.getConnectionToWrite(Globals.context);

            long numRowsAffected = connection.delete(TABLE_NAME, "id = ?", new String[]{String.valueOf(jogo.getId())});

            if (numRowsAffected > 0) {
                return true;
            }

            return false;

        }catch (Exception ex){
            Log.e(TAG, "An error while deleting the game", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    public List<Jogo> readAll() {

        SQLiteDatabase connection = DBJogo.getConnectionToRead(Globals.context);

        Cursor cursor = connection.rawQuery("SELECT * FROM jogo ", null);

        List<Jogo> jogos = new ArrayList();

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {

                        Long idjogoDb = cursor.getLong(cursor.getColumnIndex("id"));
                        Long idtreinadorDb = cursor.getLong(cursor.getColumnIndex("treinadorid"));
                        String campoDb = cursor.getString(cursor.getColumnIndex("campo"));
                        String resultadoDb = cursor.getString(cursor.getColumnIndex("resultado"));
                        String edAdvDb = cursor.getString(cursor.getColumnIndex("eqadv"));
                        String campeonadoDb = cursor.getString(cursor.getColumnIndex("campeonato"));
                        String categoriaDb = cursor.getString(cursor.getColumnIndex("categoria"));
                        String serieDb = cursor.getString(cursor.getColumnIndex("serie"));
                        String localidadeDb = cursor.getString(cursor.getColumnIndex("localidade"));
                        String horaDb = cursor.getString(cursor.getColumnIndex("hora"));
                        String jornadaDb = cursor.getString(cursor.getColumnIndex("jornada"));
                        String visitadoDb = cursor.getString(cursor.getColumnIndex("visitado"));
                        String visitanteDb = cursor.getString(cursor.getColumnIndex("visitante"));
                        String arbitro1Db = cursor.getString(cursor.getColumnIndex("arbitro1"));
                        String arbitro2Db = cursor.getString(cursor.getColumnIndex("arbitro2"));
                        String assoFootballDb = cursor.getString(cursor.getColumnIndex("assofootball"));
                        String dataDb = cursor.getString(cursor.getColumnIndex("data"));
                        String observacaoDb = cursor.getString(cursor.getColumnIndex("observacao"));
                        String prestacaoDb = cursor.getString(cursor.getColumnIndex("prestacao"));
                        String conclusaoDb = cursor.getString(cursor.getColumnIndex("conclusao"));


                        Jogo jogo = new Jogo();
                        jogo.setId(idjogoDb);
                        jogo.setTreinadorid(idtreinadorDb);
                        jogo.setCampo(campoDb);
                        jogo.setResultado(resultadoDb);
                        jogo.setEqAdv(edAdvDb);
                        jogo.setCampeonato(campeonadoDb);
                        jogo.setCategoria(categoriaDb);
                        jogo.setSerie(serieDb);
                        jogo.setLocalidade(localidadeDb);
                        jogo.setHora(horaDb);
                        jogo.setJornada(jornadaDb);
                        jogo.setVisitado(visitadoDb);
                        jogo.setVisitante(visitanteDb);
                        jogo.setArbitro1(arbitro1Db);
                        jogo.setArbitro2(arbitro2Db);
                        jogo.setAssoFootball(assoFootballDb);
                        jogo.setData(dataDb);
                        jogo.setObservacao(observacaoDb);
                        jogo.setPrestacao(prestacaoDb);
                        jogo.setConclusao(conclusaoDb);



                        jogos.add(jogo);
                        cursor.moveToNext();
                    }
                }
            }

            return jogos;

        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

    }

    public List<Jogo> readAllByDate(){
        SQLiteDatabase connection = DBJogo.getConnectionToRead(Globals.context);


        Cursor cursor = connection.rawQuery("SELECT * FROM jogo ORDER BY data DESC", null);

    List<Jogo> jogos = new ArrayList();

    try {
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                while (cursor.isAfterLast() == false) {

                    Long idjogoDb = cursor.getLong(cursor.getColumnIndex("id"));
                    Long idtreinadorDb = cursor.getLong(cursor.getColumnIndex("treinadorid"));
                    String campoDb = cursor.getString(cursor.getColumnIndex("campo"));
                    String resultadoDb = cursor.getString(cursor.getColumnIndex("resultado"));
                    String edAdvDb = cursor.getString(cursor.getColumnIndex("eqadv"));
                    String campeonadoDb = cursor.getString(cursor.getColumnIndex("campeonato"));
                    String categoriaDb = cursor.getString(cursor.getColumnIndex("categoria"));
                    String serieDb = cursor.getString(cursor.getColumnIndex("serie"));
                    String localidadeDb = cursor.getString(cursor.getColumnIndex("localidade"));
                    String horaDb = cursor.getString(cursor.getColumnIndex("hora"));
                    String jornadaDb = cursor.getString(cursor.getColumnIndex("jornada"));
                    String visitadoDb = cursor.getString(cursor.getColumnIndex("visitado"));
                    String visitanteDb = cursor.getString(cursor.getColumnIndex("visitante"));
                    String arbitro1Db = cursor.getString(cursor.getColumnIndex("arbitro1"));
                    String arbitro2Db = cursor.getString(cursor.getColumnIndex("arbitro2"));
                    String assoFootballDb = cursor.getString(cursor.getColumnIndex("assofootball"));
                    String dataDb = cursor.getString(cursor.getColumnIndex("data"));
                    String observacaoDb = cursor.getString(cursor.getColumnIndex("observacao"));
                    String prestacaoDb = cursor.getString(cursor.getColumnIndex("prestacao"));
                    String conclusaoDb = cursor.getString(cursor.getColumnIndex("conclusao"));


                    Jogo jogo = new Jogo();
                    jogo.setId(idjogoDb);
                    jogo.setTreinadorid(idtreinadorDb);
                    jogo.setCampo(campoDb);
                    jogo.setResultado(resultadoDb);
                    jogo.setEqAdv(edAdvDb);
                    jogo.setCampeonato(campeonadoDb);
                    jogo.setCategoria(categoriaDb);
                    jogo.setSerie(serieDb);
                    jogo.setLocalidade(localidadeDb);
                    jogo.setHora(horaDb);
                    jogo.setJornada(jornadaDb);
                    jogo.setVisitado(visitadoDb);
                    jogo.setVisitante(visitanteDb);
                    jogo.setArbitro1(arbitro1Db);
                    jogo.setArbitro2(arbitro2Db);
                    jogo.setAssoFootball(assoFootballDb);
                    jogo.setData(dataDb);
                    jogo.setObservacao(observacaoDb);
                    jogo.setPrestacao(prestacaoDb);
                    jogo.setConclusao(conclusaoDb);



                    jogos.add(jogo);
                    cursor.moveToNext();
                }
            }
        }

        return jogos;

    } finally {
        if (cursor != null) {
            cursor.close();
        }

        if (connection != null) {
            connection.close();
        }
    }

}


}


