package com.example.jota.androiddemo.app;

import android.content.Context;

import com.example.jota.androiddemo.managers.MgrAuthentication;
import com.example.jota.androiddemo.managers.MgrEventoJogo;
import com.example.jota.androiddemo.managers.MgrJogador;
import com.example.jota.androiddemo.managers.MgrJogadorJogo;
import com.example.jota.androiddemo.managers.MgrJogadorTreino;
import com.example.jota.androiddemo.managers.MgrJogo;
import com.example.jota.androiddemo.managers.MgrTreino;
import com.example.jota.androiddemo.managers.MgrUtilizador;

/**
 * Created by Jota on 16-05-2015.
 */
public class Globals {



    public static Context context;

    public static MgrAuthentication mgrAuthentication;
    public static MgrUtilizador mgrUtilizador;
    public static MgrJogo mgrJogo;
    public static MgrJogador mgrJogador;
    public static MgrJogadorJogo mgrJogadorJogo;
    public static MgrEventoJogo mgrEventoJogo;
    public static MgrTreino mgrTreino;
    public static MgrJogadorTreino mgrJogadorTreino;
    public static boolean firstTime=false;
    public static boolean exameMedico = true;
    public static String dbName = "android_demo.db";

    public static String SERVICE_USERS_URL = "http://aacsf.atwebpages.com/utilizador.php"; //"http://pastebin.com/raw.php?i=ehQAUtEW";
    public static String SERVICE_JOGOS_URL = "http://aacsf.atwebpages.com/jogo.php";//"http://ogirt.com/aacsf/jogo.php";//"http://pastebin.com/raw.php?i=yM8d3pmk";
    public static String SERVICE_JOGADORES_URL = "http://aacsf.atwebpages.com/jogador.php";//"http://ogirt.com/aacsf/jogador.php";//"http://pastebin.com/raw.php?i=Xh8d7g8y";
    public static String SERVICE_JOGADORES_DOS_JOGOS = "http://aacsf.atwebpages.com/jogadorjogo.php"; //"http://ogirt.com/aacsf/jogadorjogo.php";//"http://pastebin.com/raw.php?i=q1aGaK4r";
    public static String SERVICE_EVENTOS_DO_JOGO = "http://aacsf.atwebpages.com/eventojogo.php";//"http://ogirt.com/aacsf/eventojogo.php";//"http://pastebin.com/raw.php?i=vcUyKBnA";
    public static String SERVICE_TREINOS = "http://aacsf.atwebpages.com/treino.php";//"http://pastebin.com/raw.php?i=gzRsfmf7";
    public static String SERVICE_JOGADORES_TREINO = "http://aacsf.atwebpages.com/jogadortreino.php"; //"http://pastebin.com/raw.php?i=R3PXMRgv";
}
