package com.example.jota.androiddemo.model;

/**
 * Created by Jota on 11-06-2015.
 */
public class Evento {
    private String nomeevento,jogador,tempo;


    public Evento() {

    }

    public String getNomeevento() {
        return nomeevento;
    }

    public void setNomeevento(String nomeevento) {
        this.nomeevento = nomeevento;
    }

    public String getJogador() {
        return jogador;
    }

    public void setJogador(String jogador) {
        this.jogador = jogador;
    }

    public String getTempo() {
        return tempo;
    }

    public void setTempo(String tempo) {
        this.tempo = tempo;
    }
}
