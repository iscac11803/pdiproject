package com.example.jota.androiddemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageButton;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Jogador;
import com.example.jota.androiddemo.tasks.EventoDoJogoLoader;
import com.example.jota.androiddemo.tasks.JogadorDoJogoLoader;
import com.example.jota.androiddemo.tasks.JogadorDoTreinoLoader;
import com.example.jota.androiddemo.tasks.JogadorLoader;
import com.example.jota.androiddemo.tasks.JogoLoader;
import com.example.jota.androiddemo.tasks.TreinoLoader;
import com.example.jota.androiddemo.tasks.UserAccountLoader;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class MenuActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        Globals.context = getApplicationContext();


        Bundle extras = getIntent().getExtras();
        if (Globals.exameMedico) {
            List<Jogador> jogadoresteste = Globals.mgrJogador.getAllJogadores();
            ArrayList<Jogador> jogadores = dataExameMedico(jogadoresteste);
            //Boolean flag=false;
            String resultadoFinal = "";
            for (int i = 0; i < jogadores.size(); i++) {
                resultadoFinal = resultadoFinal.concat(jogadores.get(i).getNome().concat(" no dia ").concat(jogadores.get(i).getDataexame()).concat("\n"));
            }
            avisoExameMedico(resultadoFinal);
            Globals.exameMedico = false;
        }





        final Long idutilizador = extras.getLong("idutilizador");

        Button botaojogadores = (Button) findViewById(R.id.btnmenujogadores);
        botaojogadores.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intJogadores = new Intent(Globals.context, JogadoresActivityList.class);
                startActivity(intJogadores);
            }
        });
       Button botaojogo = (Button) findViewById(R.id.btnmenujogos);
        botaojogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle dataBundle = new Bundle();
                Intent intMenu = new Intent(Globals.context, JogosActivityList.class);
                dataBundle.putLong("idutilizador",idutilizador);
                intMenu.putExtras(dataBundle);
                startActivity(intMenu);
            }
        });

        Button botaotreino = (Button) findViewById(R.id.btnMenuTreino);
        botaotreino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle dataBundle = new Bundle();
                Intent intMenu = new Intent(Globals.context, MenuPrincipalTreinos.class);
                dataBundle.putLong("idutilizador", idutilizador);
                intMenu.putExtras(dataBundle);
                startActivity(intMenu);
            }
        });
        ImageButton botaosair = (ImageButton) findViewById(R.id.btnSair);
        botaosair.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder alertDialog = new AlertDialog.Builder(MenuActivity.this);

                // Setting Dialog Title
                alertDialog.setTitle("Sair da aplicação");

                // Setting Dialog Message
                alertDialog.setMessage("Tem a certeza que quer sair da aplicação?");

                // Setting Icon to Dialog
                alertDialog.setIcon(R.drawable.sair);

                // Setting Positive "Yes" Button
                alertDialog.setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        finish();

                    }
                });

                // Setting Negative "NO" Button
                alertDialog.setNegativeButton("Não", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.cancel();
                    }
                });

                // Showing Alert Message
                alertDialog.show();

            }
        });

        ImageButton botaoatualizar = (ImageButton) findViewById(R.id.btnatualizar);
        botaoatualizar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isNetworkAvailable()) {
                    Globals.mgrJogadorTreino.atualizaTudo();
                   /* Globals.mgrJogador.apagarJogadores();
                    Globals.mgrTreino.apagarTreinos();
                    Globals.mgrEventoJogo.apagarEventosDoJogo();
                    Globals.mgrJogadorJogo.apagarJogadoresDoJogo();
                    Globals.mgrJogo.apagarJogos();
                    Globals.mgrUtilizador.apagarUtilizadores();*/

                    UserAccountLoader userAccountLoader = new UserAccountLoader(Globals.context);
                    userAccountLoader.execute(Globals.SERVICE_USERS_URL);


                    JogoLoader jogoLoader = new JogoLoader(Globals.context);
                    jogoLoader.execute(Globals.SERVICE_JOGOS_URL);
                    //é iniciada uma thread com um rest service que armazema os dados contidos no url do pastebin


                    JogadorLoader jogadorLoader = new JogadorLoader(Globals.context);
                    jogadorLoader.execute(Globals.SERVICE_JOGADORES_URL);

                    JogadorDoJogoLoader jogadoresJogoLoader = new JogadorDoJogoLoader(Globals.context);
                    jogadoresJogoLoader.execute(Globals.SERVICE_JOGADORES_DOS_JOGOS);

                    EventoDoJogoLoader eventosJogoLoader = new EventoDoJogoLoader(Globals.context);
                    eventosJogoLoader.execute(Globals.SERVICE_EVENTOS_DO_JOGO);

                    TreinoLoader treinoLoader = new TreinoLoader(Globals.context);
                    treinoLoader.execute(Globals.SERVICE_TREINOS);

                    JogadorDoTreinoLoader jogadorDoTreinoLoader = new JogadorDoTreinoLoader(Globals.context);
                    jogadorDoTreinoLoader.execute(Globals.SERVICE_JOGADORES_TREINO);

                    AlertDialog alertDialog2 = new AlertDialog.Builder(
                            MenuActivity.this).create();

                    // Setting Dialog Title
                    alertDialog2.setTitle("Atualização");

                    // Setting Dialog Message
                    alertDialog2.setMessage("Atualizado com sucesso!");

                    // Setting Icon to Dialog
                    alertDialog2.setIcon(R.drawable.check);

                    // Setting OK Button
                    alertDialog2.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intMenu = new Intent(Globals.context, MenuActivity.class);
                            Bundle dataBundle = new Bundle();
                            dataBundle.putLong("idutilizador", idutilizador);
                            intMenu.putExtras(dataBundle);
                            startActivity(intMenu);
                            finish();
                        }
                    });

                    // Showing Alert Message
                    alertDialog2.show();


                } else {
                    AlertDialog alertDialog2 = new AlertDialog.Builder(
                            MenuActivity.this).create();

                    // Setting Dialog Title
                    alertDialog2.setTitle("Ligação à internet");

                    // Setting Dialog Message
                    alertDialog2.setMessage("É necessário que tenha acesso à internet para fazer atualização!");

                    // Setting Icon to Dialog
                    alertDialog2.setIcon(R.drawable.wifi);

                    // Setting OK Button
                    alertDialog2.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intMenu = new Intent(Globals.context, MenuActivity.class);
                            Bundle dataBundle = new Bundle();
                            dataBundle.putLong("idutilizador", idutilizador);
                            intMenu.putExtras(dataBundle);
                            startActivity(intMenu);
                            finish();
                        }
                    });

                    // Showing Alert Message
                    alertDialog2.show();
                }
            }
        });
    }

    public void onBackPressed() {
        // do nothing.
    }

    @Override
    protected void onResume() {
        super.onResume();


        Globals.context = getApplicationContext();


    }

    @Override
    protected void onStop() {
        super.onStop();
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

    public ArrayList<Jogador> dataExameMedico(List<Jogador> jogadoresPassados) {

        ArrayList<Jogador> jogadores = new ArrayList();

        for (int i = 0; i < jogadoresPassados.size(); i++) {
            String dataExame = jogadoresPassados.get(i).getDataexame();
            DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = format.parse(dataExame);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            Calendar calNow = Calendar.getInstance();

            Jogador jog = new Jogador();
            String nome = jogadoresPassados.get(i).getNome();
            jog.setDataexame(dataExame);
            jog.setNome(nome);

            if (cal.get(Calendar.MONTH) == calNow.get(Calendar.MONTH)) {
                jogadores.add(jog);
            }


        }

        return jogadores;
    }

    public void avisoExameMedico(String resultadoFinal) {
        final AlertDialog alertDialog2 = new AlertDialog.Builder(
                MenuActivity.this).create();

        // Setting Dialog Title
        alertDialog2.setTitle("Exames médicos");

        // Setting Dialog Message
        alertDialog2.setMessage(resultadoFinal);

        // Setting Icon to Dialog
        alertDialog2.setIcon(R.drawable.medico);

        // Setting OK Button
        alertDialog2.setButton("OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                alertDialog2.cancel();
            }
        });

        // Showing Alert Message
        alertDialog2.show();
    }
}
