package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;

import com.example.jota.androiddemo.persistance.JogadorDbAdapter;


public class SearchViewJogadoresActivity extends Activity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    public static Context context;
    private ListView mListView;
    private SearchView searchView;
    private JogadorDbAdapter mDbHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view_jogadores);
        context = getApplicationContext();
        searchView = (SearchView) findViewById(R.id.search);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);

        mListView = (ListView) findViewById(R.id.listjogadores);
        /*inspectionDate = (TextView) findViewById(R.id.inspectionDate);
        displayDate();*/

        mDbHelper = new JogadorDbAdapter(this);
        mDbHelper.open();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public boolean onQueryTextChange(String newText) {
        showResults(newText + "*");
        return false;
    }

    public boolean onQueryTextSubmit(String query) {
        showResults(query + "*");
        return false;
    }

    public boolean onClose() {
        showResults("");
        return false;
    }

    private void showResults(String query) {

        Cursor cursor = mDbHelper.searchJogador((query != null ? query.toString() : "@@@@"));

        if (cursor == null) {
            //
        } else {
            // Specify the columns we want to display in the result
            String[] from = new String[]{

                    JogadorDbAdapter.NOME};


            // Specify the Corresponding layout elements where we want the columns to go
            int[] to = new int[]{
                    R.id.txtNome};

            // Create a simple cursor adapter for the definitions and apply them to the ListView
            SimpleCursorAdapter customers = new SimpleCursorAdapter(this, R.layout.jogadorresult, cursor, from, to);
            mListView.setAdapter(customers);

            // Define the on-click listener for the list items
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    // idjogador ,nome,datanascimento,nacionalidade,nbicc,nif,nlicenca,categoria,contactomae,nomemae,contactopai,nomepai,dataexamemedico,searchdata
                    Cursor cursor = (Cursor) mListView.getItemAtPosition(position);
                    String nome = cursor.getString(cursor.getColumnIndexOrThrow("nome"));
                    String datanascimento = cursor.getString(cursor.getColumnIndexOrThrow("datanascimento"));
                    String nacionalidade = cursor.getString(cursor.getColumnIndexOrThrow("nacionalidade"));
                    String nbicc = cursor.getString(cursor.getColumnIndexOrThrow("nbicc"));
                    String nif = cursor.getString(cursor.getColumnIndexOrThrow("nif"));
                    String nlicenca = cursor.getString(cursor.getColumnIndexOrThrow("nlicenca"));
                    String categoria = cursor.getString(cursor.getColumnIndexOrThrow("categoria"));
                    String contactomae = cursor.getString(cursor.getColumnIndexOrThrow("contactomae"));
                    String nomemae = cursor.getString(cursor.getColumnIndexOrThrow("nomemae"));
                    String contactopai = cursor.getString(cursor.getColumnIndexOrThrow("contactopai"));
                    String nomepai = cursor.getString(cursor.getColumnIndexOrThrow("nomepai"));
                    String dataexamemedico = cursor.getString(cursor.getColumnIndexOrThrow("dataexamemedico"));

                    Bundle dataBundle = new Bundle();
// idjogador ,nome,datanascimento,nacionalidade,nbicc,nif,nlicenca,categoria,contactomae,nomemae,contactopai,nomepai,dataexamemedico,searchdata

                    dataBundle.putString("nome", nome);
                    dataBundle.putString("datanascimento", datanascimento);
                    dataBundle.putString("nacionalidade", nacionalidade);
                    dataBundle.putString("nbicc", nbicc);
                    dataBundle.putString("nif", nif);
                    dataBundle.putString("nlicenca", nlicenca);
                    dataBundle.putString("categoria", categoria);
                    dataBundle.putString("contactomae", contactomae);
                    dataBundle.putString("nomemae", nomemae);
                    dataBundle.putString("contactopai", contactopai);
                    dataBundle.putString("nomepai", nomepai);
                    dataBundle.putString("dataexamemedico", dataexamemedico);

                    System.out.println("dados do search : " + nome + dataexamemedico + nacionalidade + nbicc);
                    Intent intMain = new Intent(context, DadosDoJogadorActivity.class);
                    intMain.putExtras(dataBundle);
                    startActivity(intMain);
                    finish();
                }
            });
        }
    }
}

