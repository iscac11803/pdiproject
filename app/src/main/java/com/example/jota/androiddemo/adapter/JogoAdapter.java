package com.example.jota.androiddemo.adapter;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.jota.androiddemo.R;
import com.example.jota.androiddemo.model.Jogo;

import java.util.List;

/**
 * Created by Jota on 21-05-2015.
 */
public class JogoAdapter extends BaseAdapter {

    private Context context;
    private List<Jogo> jogos;

    public JogoAdapter(Context context, List<Jogo> jogos) {
        super();
        this.context = context;
        this.jogos = jogos;
    }


    @Override
    public int getCount() {
        return jogos.size();
    }

    @Override
    public Object getItem(int position) {
        return jogos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return jogos.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.list_jogos_item_novo, parent, false);
        }

        TextView elementVisitado = (TextView) convertView.findViewById(R.id.listJogoVisitado);
        TextView elementVisitante = (TextView) convertView.findViewById(R.id.listJogoVisitante);
        TextView elementResultado = (TextView) convertView.findViewById(R.id.listJogoResultado);
        TextView elementData = (TextView) convertView.findViewById(R.id.listJogoData);
        TextView elementCategoria = (TextView) convertView.findViewById(R.id.listJogoCategoria);
        TextView elementCampo = (TextView) convertView.findViewById(R.id.listJogoCampo);


        elementVisitado.setFocusable(false);
        elementVisitante.setFocusable(false);
        elementResultado.setFocusable(false);
        elementData.setFocusable(false);
        elementCategoria.setFocusable(false);
        elementCampo.setFocusable(false);

        elementVisitado.setFocusableInTouchMode(false);
        elementVisitante.setFocusableInTouchMode(false);
        elementResultado.setFocusableInTouchMode(false);
        elementData.setFocusableInTouchMode(false);
        elementCategoria.setFocusableInTouchMode(false);
        elementCampo.setFocusableInTouchMode(false);

        Jogo jogo = jogos.get(position);

        elementVisitado.setText(jogo.getVisitado());
        elementVisitante.setText(jogo.getVisitante());
        elementResultado.setText(jogo.getResultado());
        elementData.setText(jogo.getData());
        elementCategoria.setText(jogo.getCategoria());
        elementCampo.setText(jogo.getCampo());


        switch (jogo.getConclusao()) {
            case "Vitória":
                convertView.setBackgroundColor(Color.parseColor("#B95DFF2F"));
                break;
            case "Derrota":
                convertView.setBackgroundColor(Color.parseColor("#8FFF030C"));
                break;
            case "Empate":
                convertView.setBackgroundColor(Color.parseColor("#808080"));
                break;
            default:
                throw new IllegalArgumentException("Conclusão do jogo invalido: " + jogo.getConclusao());

        }

        return convertView;
    }

    }


