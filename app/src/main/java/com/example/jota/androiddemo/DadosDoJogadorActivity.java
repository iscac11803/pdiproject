package com.example.jota.androiddemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.TextView;


public class DadosDoJogadorActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dados_do_jogador);
        Bundle infopassada = getIntent().getExtras();

        String nome = infopassada.getString("nome");
        String datanascimento = infopassada.getString("datanascimento");
        String nbicc = infopassada.getString("nbicc");
        String nif = infopassada.getString("nif");
        String nlicenca = infopassada.getString("nlicenca");
        String categoria = infopassada.getString("categoria");
        String contactomae = infopassada.getString("contactomae");
        String nomemae = infopassada.getString("nomemae");
        String contactopai = infopassada.getString("contactopai");
        String nomepai = infopassada.getString("nomepai");
        String dataexamemedico = infopassada.getString("dataexamemedico");


        TextView txtnome = (TextView) findViewById(R.id.txtJogadorNome);
        txtnome.setText(nome);
        TextView txtdatanasc = (TextView) findViewById(R.id.txtJogadorDataNasc);
        txtdatanasc.setText(datanascimento);
        TextView txtnbicc = (TextView) findViewById(R.id.txtJogadorNbiCc);
        txtnbicc.setText(nbicc);
        TextView txtnlicenca = (TextView) findViewById(R.id.txtJogadorNLicenca);
        txtnlicenca.setText(nlicenca);
        TextView txtcategoria = (TextView) findViewById(R.id.txtJogadorCategoria);
        txtcategoria.setText(categoria);
        TextView txtcontactomae = (TextView) findViewById(R.id.txtJogadorContactoMae);
        txtcontactomae.setText(contactomae);
        TextView txtnomemae = (TextView) findViewById(R.id.txtJogadorNomeDaMae);
        txtnomemae.setText(nomemae);
        TextView txtcontactopai = (TextView) findViewById(R.id.txtJogadorContactoDoPai);
        txtcontactopai.setText(contactopai);
        TextView txtnomepai = (TextView) findViewById(R.id.txtJogadorNomeDoPai);
        txtnomepai.setText(nomepai);
        TextView txtdataexamemedico = (TextView) findViewById(R.id.txtJogadorDataExameMedico);
        txtdataexamemedico.setText(dataexamemedico);
        TextView txtnif = (TextView) findViewById(R.id.txtJogadorNif);
        txtnif.setText(nif);


    }


}
