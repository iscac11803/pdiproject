package com.example.jota.androiddemo;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.example.jota.androiddemo.Utility.CustomOnItemSelectedListener;
import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Treino;

import java.util.Calendar;


public class CriarTreinoActivity extends Activity {

    //vars locais
    private String localidade, campo, hora, data, categoria;
    private Spinner spinner1;
    private Button btnSubmit;
    private ImageButton btnSelectDate, btnSelectTime;
    //vars para date picker
    private DatePicker datePicker;
    private Calendar calendar;
    private TextView dateView;
    private int year, month, day;
    //vars para timepicker
    private TextView txthora;
    private DatePickerDialog.OnDateSetListener myDateListener = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
            // arg1 = year
            // arg2 = month
            // arg3 = day
            showDate(arg1, arg2 + 1, arg3);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_criar_treino);
        Globals.context = getApplicationContext();
        Bundle extras = getIntent().getExtras();
        final Long idutilizador = extras.getLong("idutilizador");
        //spinner
        addListenerOnButton(idutilizador);
        addListenerOnSpinnerItemSelection();

        dateView = (TextView) findViewById(R.id.textViewDataTreino);
        txthora = (TextView) findViewById(R.id.textViewHoraTreino);
        btnSelectTime = (ImageButton) findViewById(R.id.btnSelectTime);

        //datepicker
        calendar = Calendar.getInstance();
        year = calendar.get(Calendar.YEAR);
        month = calendar.get(Calendar.MONTH);
        day = calendar.get(Calendar.DAY_OF_MONTH);
        showDate(year, month + 1, day);
        //timepicker
        Calendar mcurrentTime = Calendar.getInstance();
        int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
        int minute = mcurrentTime.get(Calendar.MINUTE);
        showTime(hour, minute);


        btnSelectTime.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker;
                mTimePicker = new TimePickerDialog(CriarTreinoActivity.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {

                        if (selectedMinute < 10) {
                            txthora.setText(selectedHour + ":0" + selectedMinute);
                        } else
                            txthora.setText(selectedHour + ":" + selectedMinute);
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Defina a hora:");
                mTimePicker.show();

            }
        });

        btnSelectDate = (ImageButton) findViewById(R.id.imageButtonCalendar);
        btnSelectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setDate(v);
            }
        });
    }

    private void showTime(int hour, int minute) {
        if (minute < 10) {
            txthora.setText(hour + ":0" + minute);
        } else
            txthora.setText(hour + ":" + minute);
    }

    public void addListenerOnSpinnerItemSelection() {

        spinner1 = (Spinner) findViewById(R.id.spinnerCategoria);
        spinner1.setOnItemSelectedListener(new CustomOnItemSelectedListener());
    }

    public void addListenerOnButton(final Long idutilizador) {


        btnSubmit = (Button) findViewById(R.id.btnSubmit);

        btnSubmit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                EditText localidadetv = (EditText) findViewById(R.id.txtFieldCriarTreinoLocalidade);
                spinner1 = (Spinner) findViewById(R.id.spinnerCategoria);
                TextView datatv = (TextView) findViewById(R.id.textViewDataTreino);
                EditText campotv = (EditText) findViewById(R.id.txtFieldCriarTreinoCampo);
                TextView horatv = (TextView) findViewById(R.id.textViewHoraTreino);


                localidade = localidadetv.getText().toString();
                campo = campotv.getText().toString();
                categoria = String.valueOf(spinner1.getSelectedItem());
                hora = horatv.getText().toString();
                data = datatv.getText().toString();

                if (localidade.equals("") || campo.equals("")) {
                    AlertDialog alertDialog = new AlertDialog.Builder(
                            CriarTreinoActivity.this).create();

                    // Setting Dialog Title
                    alertDialog.setTitle("Campo inválido");

                    // Setting Dialog Message
                    alertDialog.setMessage("Categoria ou campo inválido");

                    // Setting Icon to Dialog
                    alertDialog.setIcon(R.drawable.exclamation);

                    // Setting OK Button
                    alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intMenu = new Intent(Globals.context, CriarTreinoActivity.class);
                            Bundle dataBundle = new Bundle();
                            dataBundle.putLong("idutilizador", idutilizador);
                            intMenu.putExtras(dataBundle);
                            startActivity(intMenu);
                        }
                    });

                    // Showing Alert Message
                    alertDialog.show();
                } else {
                Treino treino = new Treino();
                treino.setLocalidade(localidade);
                treino.setCampo(campo);
                treino.setCategoria(categoria);
                treino.setHora(hora);
                treino.setData(data);

                Globals.mgrTreino.addTreino(treino);
                Long idtreino = Globals.mgrTreino.getIdTreino(treino);
                treino.setId(idtreino);


                if (idtreino != null) {
                    Globals.mgrTreino.uptateTreinoById(treino);
                    Bundle dataBundle = new Bundle();
                    Intent intMenu = new Intent(Globals.context, PresencasTreinoActivity.class);

                    dataBundle.putLong("idutilizador", idutilizador);
                    dataBundle.putLong("idtreino", idtreino);
                    dataBundle.putString("categoria", categoria);
                    dataBundle.putString("localidade", localidade);
                    dataBundle.putString("campo", campo);
                    dataBundle.putString("data", data);
                    dataBundle.putString("hora", hora);
                    intMenu.putExtras(dataBundle);

                    startActivity(intMenu);
                    finish();
                } else
                    Toast.makeText(Globals.context, "Treino já existe", Toast.LENGTH_SHORT).show();
                /*System.out.println(localidade);
                System.out.println(campo);
                System.out.println(categoria);
                System.out.println(hora);
                System.out.println(data);*/
                }
            }

        });
    }

    //datepicker
    @SuppressWarnings("deprecation")
    public void setDate(View view) {
        showDialog(999);
        Toast.makeText(getApplicationContext(), "ca", Toast.LENGTH_SHORT)
                .show();
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        // TODO Auto-generated method stub
        if (id == 999) {
            return new DatePickerDialog(this, myDateListener, year, month, day);
        }
        return null;
    }

    private void showDate(int year, int month, int day) {

        if (day < 10 && month < 10) {
            dateView.setText(new StringBuilder().append(year).append("-0")
                    .append(month).append("-0").append(day));

        } else if (day < 10) {
            dateView.setText(new StringBuilder().append(year).append("-")
                    .append(month).append("-0").append(day));
        } else if (month < 10) {
            dateView.setText(new StringBuilder().append(year).append("-0")
                    .append(month).append("-").append(day));
        } else {
            dateView.setText(new StringBuilder().append(year).append("-")
                    .append(month).append("-").append(day));
        }


    }
}
