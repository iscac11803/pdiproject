package com.example.jota.androiddemo.managers;

import android.util.Log;

import com.example.jota.androiddemo.model.JogadorTreino;
import com.example.jota.androiddemo.persistance.PJogadorTreino;

import java.util.List;

/**
 * Created by Jota on 24-06-2015.
 */
public class MgrJogadorTreino {

    private static String TAG = "MgrJogadoresTreino";

    private PJogadorTreino pJogadorTreino;

    public MgrJogadorTreino() {
        pJogadorTreino = new PJogadorTreino();

    }


    public boolean addJogadorTreino(JogadorTreino jogadortreino) {


        JogadorTreino existingJogadorTreino = pJogadorTreino.read(jogadortreino.getId());

        if (existingJogadorTreino == null) {
            return pJogadorTreino.create(jogadortreino);
        } else {
            Log.i(TAG, "The player[" + jogadortreino.getId() + "] already exists on that training");
            return false;
        }

    }

    public void addJogadorTreinoPresenca(Long idjogador, Long idtreino) {
        pJogadorTreino.createJogadoresNoTreino(idjogador, idtreino);
    }

    public boolean apagarJogadoresDoTreino(Long idtreino) {
        if (pJogadorTreino.deleteJogadoresDoTreino(idtreino))
            return true;
        else
            return false;
    }

    public List<JogadorTreino> getJogadoresDoTreino(Long idtreino) {

        List<JogadorTreino> jogadoresDoTreino = pJogadorTreino.readJogadoresDoTreino(idtreino);
        Log.d(TAG, "Found [" + jogadoresDoTreino.size() + "] players in that training");
        return jogadoresDoTreino;

    }

    public boolean atualizaTudo() {
        if (pJogadorTreino.atualizaTudo()) {
            return true;
        } else {
            System.out.println("Erro ao apagar tudo");
            return false;
        }
    }
}