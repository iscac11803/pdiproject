package com.example.jota.androiddemo.persistance;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;

/**
 * Created by Jota on 16-05-2015.
 */
public class DBUtil extends SQLiteOpenHelper {

    private static final String TAG = "DBUtil";

    private static final int VERSION = 1;

    private static final String DB_NAME = "android_demo.db";


    private static final String CREATE_UTILIZADOR =
            "CREATE TABLE utilizador ( " +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "email TEXT not null, " +
                    "senha TEXT not null " +
                    ");";

    private static final String CREATE_JOGO =
            "CREATE VIRTUAL TABLE jogo USING fts4(id INTEGER PRIMARY KEY ASC,treinadorid,campo,resultado,eqadv,campeonato,categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assofootball,data,observacao,prestacao,conclusao text,searchdata,UNIQUE(id));";
    private static final String CREATE_TREINO = "CREATE VIRTUAL TABLE treino USING fts4(id INTEGER PRIMARY KEY ASC,localidade,categoria,data,campo,hora,searchdata,observacao,UNIQUE(id))";
    private static final String CREATE_JOGADOR = "CREATE VIRTUAL TABLE jogador USING fts4(id INTEGER PRIMARY KEY ASC,nome,datanascimento,nacionalidade,nbicc,nif,nlicenca,categoria,contactomae,nomemae,contactopai,nomepai,dataexamemedico,searchdata,UNIQUE(id))"; //image blob not null
    //private static final String CREATE_JOGADOR_DE_JOGO="CREATE VIRTUAL TABLE jogadorjogo USING fts4(idjogadorjogo,idjogadorfk,idjogofk,idevento,prestacao,observacao,FOREIGN KEY (idjogadorfk) REFERENCES jogador(idjogador),FOREIGN KEY (idjogo) REFERENCES jogo(idjogo),UNIQUE(idjogadorjogo))";
    private static final String CREATE_JOGADOR_DE_JOGO = "CREATE TABLE jogadorjogo(id integer primary key autoincrement,jogadorid integer,jogoid integer,idevento integer,prestacao text,observacao text,FOREIGN KEY(jogadorid) REFERENCES jogador(id),FOREIGN KEY(jogoid) REFERENCES jogo(id))";
    private static final String CREATE_TIPO_DE_EVENTO = "CREATE TABLE tipoevento(id integer primary key autoincrement,nome text)";
    private static final String INSERIR_DADOS_TIPO_EVENTO = "INSERT INTO tipoevento SELECT '1' AS id, 'Cartão Amarelo' AS nome UNION SELECT '2', 'Cartão Vermelho' UNION SELECT '3', 'Golo' UNION SELECT '4', 'Entrada' UNION SELECT '5', 'Saida'";
    private static final String CREATE_EVENTOS_DO_JOGO = "CREATE TABLE eventojogo(id integer primary key autoincrement,tipodeeventoid integer,tempo integer,jogadorjogoid integer,FOREIGN KEY (tipodeeventoid) REFERENCES tipoevento(id),FOREIGN KEY (jogadorjogoid) REFERENCES jogadorjogo(id))";
    private static final String CREATE_JOGADOR_TREINO = "CREATE TABLE jogadortreino(id integer primary key autoincrement,jogadorid integer,treinoid integer,FOREIGN KEY(jogadorid) REFERENCES jogador(id),FOREIGN KEY(treinoid) REFERENCES treino(id))";
    public DBUtil(Context context) {
        super(context, DB_NAME, null, VERSION);

    }

    public static SQLiteDatabase getConnectionToWrite(Context context){

        SQLiteDatabase connection = null;

        DBUtil dbUtil = new DBUtil(Globals.context);

        return dbUtil.getWritableDatabase();

    }

    public static SQLiteDatabase getConnectionToRead(Context context){

        SQLiteDatabase connection = null;

        DBUtil dbUtil = new DBUtil(Globals.context);

        return dbUtil.getReadableDatabase();

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_UTILIZADOR);
        db.execSQL(CREATE_JOGO);
        db.execSQL(CREATE_JOGADOR);
        db.execSQL(CREATE_JOGADOR_DE_JOGO);
        db.execSQL(CREATE_TIPO_DE_EVENTO);
        db.execSQL(INSERIR_DADOS_TIPO_EVENTO);
        db.execSQL(CREATE_EVENTOS_DO_JOGO);
        db.execSQL(CREATE_TREINO);
        db.execSQL(CREATE_JOGADOR_TREINO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.w(TAG, "Upgrading database from version " + oldVersion +
                " to " + newVersion + ", which will destroy all old data");

        db.execSQL("DROP TABLE IF EXISTS utilizador");
        db.execSQL("DROP TABLE IF EXISTS jogo");
        db.execSQL("DROP TABLE IF EXISTS jogador");
        db.execSQL("DROP TABLE IF EXISTS jogadorjogo");
        db.execSQL("DROP TABLE IF EXISTS tipoevento");
        db.execSQL("DROP TABLE IF EXISTS treino");
        db.execSQL("DROP TABLE IF EXISTS jogadortreino");
        db.execSQL("DROP TABLE IF EXISTS eventojogo");

        onCreate(db);
    }

}
