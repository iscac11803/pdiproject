package com.example.jota.androiddemo.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.commons.GenericRestClient;
import com.example.jota.androiddemo.model.Utilizador;
import com.example.jota.androiddemo.parser.UtilizadorDataParser;
import com.example.jota.androiddemo.parser.UtilizadorParser;
import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 18-05-2015.
 */
public class UserAccountLoader extends AsyncTask<String, Integer, Boolean> {

    private final String TAG = "UsersAccountLoader";
    private final Context context;

    public UserAccountLoader(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        //Fazer qualquer coisa antes de iniciar a
        // tarefa (util para iniciar uma caixa de progresso)
    }

    @Override
    protected Boolean doInBackground(String... params) {
        //Faz coisas em background

        List<Utilizador> utilizadors = new ArrayList();

        String serviceUrl = params[0];

        GenericRestClient rest = new GenericRestClient();
        rest.setLocation(serviceUrl);

        try {
            InputStreamReader result = rest.invoke();
            if (result != null){
                Gson parser = new Gson();
                UtilizadorDataParser resultParsed = parser.fromJson(result, UtilizadorDataParser.class);
                if (resultParsed != null){
                    if (resultParsed.getUtilizadores() != null) {
                        for (UtilizadorParser userParser : resultParsed.getUtilizadores()) {
                            Utilizador utilizador = new Utilizador();
                            utilizador.setId(userParser.getId());
                            utilizador.setEmail(userParser.getEmail());
                            utilizador.setSenha(userParser.getSenha());


                            utilizadors.add(utilizador);

                        }
                    }
                    if (utilizadors != null && utilizadors.size() > 0) {

                        for (Utilizador utilizador : utilizadors) {
                            boolean addResult = Globals.mgrUtilizador.addUser(utilizador);


                            if (addResult == false){
                                Log.e(TAG, "Error while adding user[" + utilizador.getId() + "]");
                            }
                        }

                        return true;
                    }
                }
            }

            return false;

        } catch (Exception e ) {
            Log.e(TAG, "Error while loading train stations from service [" + Globals.SERVICE_USERS_URL + "]", e);
            return  null;
        }
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        //Fazer qualquer coisa depois de terminar a tarefa
        // (util para iniciar uma caixa de progresso)
    }

}
