package com.example.jota.androiddemo.persistance;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * Created by Jota on 19-06-2015.
 */
public class JogadorDbAdapter {
    public static final String NOME = "nome";
    private static final String TAG = "JogadorDbAdapter";
    private static final String DATABASE_NAME = "android_demo.db";
    private static final String FTS_VIRTUAL_TABLE = "jogador";
    private static final int DATABASE_VERSION = 1;
    //Create a FTS3 Virtual Table for fast searches
    private static final String DATABASE_CREATE = "CREATE VIRTUAL TABLE jogador USING fts4(idjogador integer primary key autoincrement,nome,datanascimento,nacionalidade,nbicc,nif,nlicenca,categoria,contactomae,nomemae,contactopai,nomepai,dataexamemedico,searchdata,UNIQUE(idjogador))";
    private final Context mCtx;
    private DatabaseHelper mDbHelper;
    private SQLiteDatabase mDb;

    public JogadorDbAdapter(Context ctx) {
        this.mCtx = ctx;
    }

    public JogadorDbAdapter open() throws SQLException {
        mDbHelper = new DatabaseHelper(mCtx);
        mDb = mDbHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public Cursor getJogadorById(Long idjogador) {

        String query = "SELECT docid as _id,* from jogador where idjogador MATCH'" + idjogador + "'";
        Log.w(TAG, query);
        Cursor mCursor = mDb.rawQuery(query, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }


    public Cursor searchJogador(String inputText) throws SQLException {
        Log.w(TAG, inputText);
        String query = "SELECT rowid as _id,* from jogador where  nome MATCH '" + inputText + "'";


        Log.w(TAG, query);
        Cursor mCursor = mDb.rawQuery(query, null);

        if (mCursor != null) {
            mCursor.moveToFirst();
        }
        return mCursor;

    }

    public boolean deleteAllJogadores() {

        int doneDelete = 0;
        doneDelete = mDb.delete(FTS_VIRTUAL_TABLE, null, null);
        Log.w(TAG, Integer.toString(doneDelete));
        return doneDelete > 0;

    }

    private static class DatabaseHelper extends SQLiteOpenHelper {

        DatabaseHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }


        @Override
        public void onCreate(SQLiteDatabase db) {
            /*Log.w(TAG, DATABASE_CREATE);
            db.execSQL(DATABASE_CREATE);*/
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            /*Log.w(TAG, "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
            db.execSQL("DROP TABLE IF EXISTS " + FTS_VIRTUAL_TABLE);
            onCreate(db);*/
        }
    }


}
