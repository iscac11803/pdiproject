package com.example.jota.androiddemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import com.example.jota.androiddemo.adapter.EventoJogoAdapter;
import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Evento;

import java.util.List;


public class EventosDoJogoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_eventos_do_jogo);
        Globals.context=getApplicationContext();

        Bundle infopassada = getIntent().getExtras();


        Long idjogo=infopassada.getLong("idjogo");


        List<Evento> listaDeEventos = Globals.mgrEventoJogo.getAllEventosDoJogo(idjogo);

        ListView listViewEventosDoJogo = (ListView)findViewById(R.id.lista_eventos_jogo);


        EventoJogoAdapter eventoJogoAdapter = new EventoJogoAdapter(Globals.context, listaDeEventos);
        listViewEventosDoJogo.setAdapter(eventoJogoAdapter);
    }


}
