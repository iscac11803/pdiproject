package com.example.jota.androiddemo.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.commons.GenericRestClient;
import com.example.jota.androiddemo.model.EventoJogo;
import com.example.jota.androiddemo.parser.EventoJogoDataParser;
import com.example.jota.androiddemo.parser.EventoJogoParser;
import com.google.gson.Gson;

import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 11-06-2015.
 */
public class EventoDoJogoLoader extends AsyncTask<String, Integer, Boolean> {

    private final String TAG = "EventosDoJogoLoader";
    private final Context context;


    public EventoDoJogoLoader(Context context) {
        this.context = context;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();

    }

    @Override
    protected Boolean doInBackground(String... params) {
        //Faz coisas em background
        List<EventoJogo> eventosjogo = new ArrayList();

        String serviceUrl = params[0];

        GenericRestClient rest = new GenericRestClient();
        rest.setLocation(serviceUrl);

        try {
            InputStreamReader result = rest.invoke();
            if (result != null) {
                Gson parser = new Gson();
                EventoJogoDataParser resultParsed = parser.fromJson(result, EventoJogoDataParser.class);
                if (resultParsed != null) {
                    if (resultParsed.getEventosjogo() != null) {
                        for (EventoJogoParser eventoJogoParser : resultParsed.getEventosjogo()) {
                            // idjogo,idtreinador, campo,resultado,eqAdv,campeonato, categoria,serie,localidade,hora,jornada,visitado,visitante,arbitro1,arbitro2,assoFootball,data,observacao,prestacao;
                            EventoJogo eventojogo = new EventoJogo();
                            eventojogo.setIdeventojogo(eventoJogoParser.getId());
                            eventojogo.setIdtipoevento(eventoJogoParser.getTipodeeventoid());
                            eventojogo.setIdjogadorjogo(eventoJogoParser.getJogadorjogoid());
                            eventojogo.setTempo(eventoJogoParser.getTempo());

                            eventosjogo.add(eventojogo);
                        }
                    }
                    if (eventosjogo != null && eventosjogo.size() > 0) {
                        for (EventoJogo eventoJogo : eventosjogo) {

                            boolean addResult = Globals.mgrEventoJogo.addEventoJogo(eventoJogo);
                            if (addResult == false) {
                                Log.e(TAG, "Error while adding event[" + eventoJogo.getIdeventojogo() + "]");
                            }
                        }

                        return true;
                    }
                }
            }

            return false;

        } catch (Exception e) {
            Log.e(TAG, "Error while loading train stations from service [" + Globals.SERVICE_EVENTOS_DO_JOGO + "]", e);
            return null;
        }
    }


    @Override
    protected void onPostExecute(Boolean aBoolean) {
        super.onPostExecute(aBoolean);
        //Fazer qualquer coisa depois de terminar a tarefa
        // (util para iniciar uma caixa de progresso)
    }

}

