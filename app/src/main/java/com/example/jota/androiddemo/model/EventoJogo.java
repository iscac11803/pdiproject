package com.example.jota.androiddemo.model;

/**
 * Created by Jota on 08-06-2015.
 */
public class EventoJogo {
    private Long ideventojogo,idtipoevento,idjogadorjogo;
    private Long tempo;

    public EventoJogo() {

    }

    public Long getIdeventojogo() {
        return ideventojogo;
    }

    public void setIdeventojogo(Long ideventojogo) {
        this.ideventojogo = ideventojogo;
    }

    public Long getIdtipoevento() {
        return idtipoevento;
    }

    public void setIdtipoevento(Long idtipoevento) {
        this.idtipoevento = idtipoevento;
    }

    public Long getIdjogadorjogo() {
        return idjogadorjogo;
    }

    public void setIdjogadorjogo(Long idjogadorjogo) {
        this.idjogadorjogo = idjogadorjogo;
    }

    public Long getTempo() {
        return tempo;
    }

    public void setTempo(Long tempo) {
        this.tempo = tempo;
    }
}
