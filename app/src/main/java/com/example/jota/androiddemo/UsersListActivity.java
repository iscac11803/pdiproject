package com.example.jota.androiddemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import com.example.jota.androiddemo.adapter.UtilizadorAdapter;
import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Utilizador;

import java.util.List;

public class UsersListActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jogos);

        Globals.context = getApplicationContext();

        ListView listUsers = (ListView) findViewById(R.id.list);

        List<Utilizador> utilizadors = Globals.mgrUtilizador.getAllUsers();
        UtilizadorAdapter adapter = new UtilizadorAdapter(Globals.context, utilizadors);
        listUsers.setAdapter(adapter);

    }


}
