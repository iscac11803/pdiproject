package com.example.jota.androiddemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ListView;

import com.example.jota.androiddemo.adapter.JogadorJogoAdapter;
import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Jogador;

import java.util.List;


public class JogadoresDoTreinoActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        Globals.context = getApplicationContext();

        setContentView(R.layout.activity_jogadores_do_treino);
        Bundle infopassada = getIntent().getExtras();
        Long idtreino = infopassada.getLong("idtreino");
        System.out.println(idtreino);

        List<Jogador> jogadores = Globals.mgrJogador.getAllJogadoresDoTreino(idtreino);

        ListView listJogadores = (ListView) findViewById(R.id.IdListaJogadoresTreino);


        JogadorJogoAdapter adapter = new JogadorJogoAdapter(Globals.context, jogadores);
        listJogadores.setAdapter(adapter);
    }


}
