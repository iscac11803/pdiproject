package com.example.jota.androiddemo.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.jota.androiddemo.R;
import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Evento;

import java.util.List;

/**
 * Created by Jota on 11-06-2015.
 */
public class EventoJogoAdapter extends BaseAdapter {

    private Context context;
    private List<Evento> eventos;

    public EventoJogoAdapter(Context context, List<Evento> eventos) {
        super();
        this.context = context;
        this.eventos = eventos;
    }

    @Override
    public int getCount() {
        return eventos.size();
    }

    @Override
    public Object getItem(int position) {
        return eventos.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(convertView==null)
        {
            LayoutInflater mInflater =
                    (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = mInflater.inflate(R.layout.linha_da_lista_eventos_do_jogo, parent,false);
        }



        TextView elementEvento= (TextView)convertView.findViewById(R.id.idlinhaevento);
        TextView elementEventoTempo= (TextView)convertView.findViewById(R.id.textViewTempo);
        ImageView elementEventoImagem= (ImageView)convertView.findViewById(R.id.eventosImagemTipoEvento);



        Evento evento = eventos.get(position);
        String linhaevento="";

        String condicao = evento.getNomeevento();

        if(condicao.equals("Golo") || condicao.equals("Entrada") || condicao.equals("Saida"))
        {
            linhaevento = evento.getNomeevento()+" do jogador "+evento.getJogador();
        }
        else {
            linhaevento = evento.getNomeevento() + " ao jogador " + evento.getJogador();
        }

        elementEventoTempo.setText(evento.getTempo() + "'");
        elementEvento.setText(linhaevento);



        switch (evento.getNomeevento()){
            case "Cartão Amarelo":
                elementEventoImagem.setImageDrawable(Globals.context.getResources().getDrawable(R.drawable.cartaoamarelo));
                break;
            case "Cartão Vermelho":
                elementEventoImagem.setImageDrawable(Globals.context.getResources().getDrawable(R.drawable.cartaovermelho));
                break;
            case "Golo":
                elementEventoImagem.setImageDrawable(Globals.context.getResources().getDrawable(R.drawable.golo));
                break;
            case "Entrada":
                elementEventoImagem.setImageDrawable(Globals.context.getResources().getDrawable(R.drawable.setaentrada));
                break;
            case "Saida":
                elementEventoImagem.setImageDrawable(Globals.context.getResources().getDrawable(R.drawable.setasaida));
                break;
            default:

        }

        return convertView;
    }
}
