package com.example.jota.androiddemo.commons;

import android.util.Base64;
import android.util.Log;

import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;

/**
 * Created by Jota on 18-05-2015.
 */
public class GenericRestClient {

    protected final static String TAG = "GenericRestClient";

    protected String location;
    protected Charset encoding = Charset.defaultCharset();
    protected int connectionTimeout = 5000;
    protected int readTimeout = 5000;
    protected String contentType = "application/x-www-form-urlencoded";
    protected boolean requiresAuthentication = false;
    protected String username;
    protected String password;

    public GenericRestClient() {
    }

    /**
     * Evaluates if the request has the minimum requirements to initiate the
     * communication
     *
     * @return boolean True if valid, False otherwise
     */
    private boolean isValid() {
        if (location == null ) {
            Log.e(TAG, "Cannot invoke a service request without the service location");
            return false;
        }
        if (requiresAuthentication == true && (username == null || password == null)) {
            Log.e(TAG, "Cannot invoke a service [" + location + "] that requires authentication without providing the username and password");
            return false;
        }

        return true;
    }

    public InputStreamReader invoke()
            throws Exception {

        if (!isValid()) {
            return null;
        }

        URL url = new URL(location);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        if (connectionTimeout > 0) {
            connection.setConnectTimeout(connectionTimeout);
        }

        if (readTimeout > 0) {
            connection.setReadTimeout(readTimeout);
        }

        connection.setUseCaches(false);
        connection.setRequestMethod("GET");
        connection.setConnectTimeout(readTimeout);
        connection.setRequestProperty("Accept", "application/xml");

        if (requiresAuthentication) {
            String authString = username + ":" + password;
            byte[] authEncBytes = Base64.encode(authString.getBytes(), Base64.DEFAULT);
            String authStringEnc = new String(authEncBytes);
            connection.setRequestProperty("Authorization", "Basic " + authStringEnc);
        }

        Log.d(TAG, "Will invoke the service[" + location + "]");
        long timer = System.currentTimeMillis();

        boolean success = false;
        try {

            connection.connect();

            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                success = true;
                return new InputStreamReader((InputStream) connection.getContent(), encoding);
            }

        } catch (Exception e) {
            Log.e(TAG, "Error while requesting the service [" + location + "], took [" + (System.currentTimeMillis() - timer) + "]ms", e);
            throw e;
        } finally {

            long took = System.currentTimeMillis() - timer;

            if (success) {
                Log.d(TAG, "Successfully requested the service[" + location + "], took [" + took + "]ms");
            } else {
                Log.e(TAG, "Error while requesting the service [" + location + "], took [" + took + "]ms");
            }
        }

        return null;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Charset getEncoding() {
        return encoding;
    }

    public void setEncoding(Charset encoding) {
        this.encoding = encoding;
    }

    public int getConnectionTimeout() {
        return connectionTimeout;
    }

    public void setConnectionTimeout(int connectionTimeout) {
        this.connectionTimeout = connectionTimeout;
    }

    public int getReadTimeout() {
        return readTimeout;
    }

    public void setReadTimeout(int readTimeout) {
        this.readTimeout = readTimeout;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public boolean requiresAuthentication() {
        return requiresAuthentication || username != null && password != null;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }


}
