package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;

import com.example.jota.androiddemo.app.Globals;


public class MenuPrincipalTreinos extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_principal_treinos);
        Bundle extras = getIntent().getExtras();
        Globals.context = getApplicationContext();
        final Long idutilizador = extras.getLong("idutilizador");


        Button botaotreino = (Button) findViewById(R.id.btnMenuTreino);
        botaotreino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle dataBundle = new Bundle();
                Intent intMenu = new Intent(Globals.context, TreinoListActivity.class);
                dataBundle.putLong("idutilizador", idutilizador);
                intMenu.putExtras(dataBundle);
                startActivity(intMenu);
            }
        });
        Button botaocriartreino = (Button) findViewById(R.id.btnMenuAddTreino);
        botaocriartreino.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle dataBundle = new Bundle();
                Intent intMenu = new Intent(Globals.context, CriarTreinoActivity.class);
                dataBundle.putLong("idutilizador", idutilizador);
                intMenu.putExtras(dataBundle);
                startActivity(intMenu);
            }
        });
    }


}
