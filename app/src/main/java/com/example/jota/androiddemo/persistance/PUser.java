package com.example.jota.androiddemo.persistance;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Utilizador;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jota on 23-04-2015.
 */
//classe responsavel por guardar (persistir) os dados dos utilizadores
public class PUser {
    private static final String CREATE_UTILIZADOR =
            "CREATE TABLE utilizador ( " +
                    "id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
                    "email TEXT not null, " +
                    "senha TEXT not null " +
                    ");";

    private static final String TAG = "PUsers";

    private static final String TABLE_NAME = "utilizador";


    public PUser() {
    }

    public boolean create(Utilizador utilizador) {

        SQLiteDatabase connection = null;

        try {

            connection = DBUtil.getConnectionToWrite(Globals.context);

            ContentValues values = new ContentValues();
            values.put("email", utilizador.getEmail());
            values.put("senha", utilizador.getSenha());

            long insertedId = connection.insert(TABLE_NAME, null, values);

            if (insertedId <= 0) {
                Log.e(PUser.class.getName(), "Error while inserting the user");
                return false;
            } else {
                utilizador.setId(insertedId);
            }

            return true;

        }catch (Exception ex){
            Log.e(TAG, "An error while creating user", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }
    public void primeiraVez() {
        SQLiteDatabase connection = null;

        try {

            connection = DBUtil.getConnectionToWrite(Globals.context);
            connection.execSQL(CREATE_UTILIZADOR);
        } catch (Exception ex) {
            Log.e(TAG, "Erro a iniciar pela primeira vez", ex);
        }finally {
            Globals.firstTime=true;
            connection.close();
        }

        }


    public Utilizador read(String email, String senha) {

        SQLiteDatabase connection = DBUtil.getConnectionToRead(Globals.context);

        Cursor cursor = connection.
                rawQuery("SELECT * " +
                                "FROM utilizador " +
                                "WHERE email = ? AND senha = ?",
                        new String[]{email, senha});

        try {
            if (cursor != null) {
                if (cursor.moveToNext()) {
                    Long idDb = cursor.getLong(cursor.getColumnIndex("id"));
                    String emailDb = cursor.getString(cursor.getColumnIndex("email"));
                    String senhaDb = cursor.getString(cursor.getColumnIndex("senha"));

                    Utilizador utilizador = new Utilizador();
                    utilizador.setId(idDb);
                    utilizador.setEmail(emailDb);
                    utilizador.setSenha(senhaDb);
                    return utilizador;
                }
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

        return null;
    }

    public boolean update(Utilizador utilizador) {

        SQLiteDatabase connection = null;

        try {

            connection = DBUtil.getConnectionToWrite(Globals.context);

            ContentValues values = new ContentValues();

            if (utilizador.getEmail() != null) {
                values.put("email", utilizador.getEmail());
            }
            if (utilizador.getSenha() != null) {
                values.put("senha", utilizador.getSenha());
            }


            long numRowAffected = connection.update(TABLE_NAME, values, "id = ?", new String[]{String.valueOf(utilizador.getId())});

            if (numRowAffected > 0) {
                return true;
            }

            return false;

        }catch (Exception ex){
            Log.e(TAG, "An error while updating user", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }

    public boolean delete(Utilizador utilizador) {
        SQLiteDatabase connection = null;

        try {

            connection = DBUtil.getConnectionToWrite(Globals.context);

            long numRowsAffected = connection.delete(TABLE_NAME, "id = ?", new String[]{String.valueOf(utilizador.getId())});

            if (numRowsAffected > 0) {
                return true;
            }

            return false;

        }catch (Exception ex){
            Log.e(TAG, "An error while deleting the user", ex);
            return false;
        } finally {
            if (connection != null) {
                connection.close();
            }
        }
    }


    public List<Utilizador> readAll() {

        SQLiteDatabase connection = DBUtil.getConnectionToRead(Globals.context);

        Cursor cursor = connection.rawQuery("SELECT * FROM utilizador ", null);

        List<Utilizador> utilizadors = new ArrayList();

        try {
            if (cursor != null) {
                if (cursor.moveToFirst()) {
                    while (cursor.isAfterLast() == false) {
                        Long idDb = cursor.getLong(cursor.getColumnIndex("id"));
                        String emailDb = cursor.getString(cursor.getColumnIndex("email"));
                        String senhaDb = cursor.getString(cursor.getColumnIndex("senha"));

                        Utilizador utilizador = new Utilizador();
                        utilizador.setId(idDb);
                        utilizador.setEmail(emailDb);
                        utilizador.setSenha(senhaDb);

                        utilizadors.add(utilizador);
                        cursor.moveToNext();
                    }
                }
            }

            return utilizadors;

        } finally {
            if (cursor != null) {
                cursor.close();
            }

            if (connection != null) {
                connection.close();
            }
        }

    }

}
