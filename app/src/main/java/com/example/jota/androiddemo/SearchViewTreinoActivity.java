package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SearchView;
import android.widget.SimpleCursorAdapter;

import com.example.jota.androiddemo.persistance.JogoDbAdapter;
import com.example.jota.androiddemo.persistance.TreinoDbAdapter;

/**
 * Created by Jota on 25-06-2015.
 */
public class SearchViewTreinoActivity extends Activity implements SearchView.OnQueryTextListener,
        SearchView.OnCloseListener {

    public static Context context;
    private ListView mListView;
    private SearchView searchView;
    private TreinoDbAdapter mDbHelper;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_view_treinos);
        context = getApplicationContext();
        searchView = (SearchView) findViewById(R.id.search);
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(this);
        searchView.setOnCloseListener(this);

        mListView = (ListView) findViewById(R.id.listtreinoscoiso);
        /*inspectionDate = (TextView) findViewById(R.id.inspectionDate);
        displayDate();*/

        mDbHelper = new TreinoDbAdapter(this);
        mDbHelper.open();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mDbHelper != null) {
            mDbHelper.close();
        }
    }

    public boolean onQueryTextChange(String newText) {
        showResults(newText + "*");
        return false;
    }

    public boolean onQueryTextSubmit(String query) {
        showResults(query + "*");
        return false;
    }

    public boolean onClose() {
        showResults("");
        return false;
    }

    private void showResults(String query) {

        Cursor cursor = mDbHelper.searchTreino((query != null ? query.toString() : "@@@@"));

        if (cursor == null) {
            //
        } else {
            // Specify the columns we want to display in the result
            String[] from = new String[]{

                    JogoDbAdapter.DATA,
                    JogoDbAdapter.HORA,
                    JogoDbAdapter.CATEGORIA};


            // Specify the Corresponding layout elements where we want the columns to go
            int[] to = new int[]{
                    R.id.textViewSearchedTreinoCategoria,
                    R.id.textViewSearchedTreinoHora,
                    R.id.textViewSearchedTreinoData};


            // Create a simple cursor adapter for the definitions and apply them to the ListView
            SimpleCursorAdapter customers = new SimpleCursorAdapter(this, R.layout.treinoresult, cursor, from, to);
            mListView.setAdapter(customers);

            // Define the on-click listener for the list items
            mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {


                    Cursor cursor = (Cursor) mListView.getItemAtPosition(position);
                    String campo = cursor.getString(cursor.getColumnIndexOrThrow("campo"));
                    String categoria = cursor.getString(cursor.getColumnIndexOrThrow("categoria"));
                    String localidade = cursor.getString(cursor.getColumnIndexOrThrow("localidade"));
                    String hora = cursor.getString(cursor.getColumnIndexOrThrow("hora"));
                    String data = cursor.getString(cursor.getColumnIndexOrThrow("data"));
                    Long idjogo = cursor.getLong(cursor.getColumnIndex("_id"));

                    Bundle dataBundle = new Bundle();

                    dataBundle.putString("campo", campo);
                    dataBundle.putString("data", data);
                    dataBundle.putString("categoria", categoria);
                    dataBundle.putString("localidade", localidade);
                    dataBundle.putString("hora", hora);
                    dataBundle.putLong("idjogo", idjogo);
                    Intent intMain = new Intent(context, DadosTreinoActivity.class);
                    intMain.putExtras(dataBundle);
                    startActivity(intMain);
                    finish();

                }
            });
        }
    }


}
