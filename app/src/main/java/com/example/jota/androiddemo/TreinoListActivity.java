package com.example.jota.androiddemo;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.example.jota.androiddemo.adapter.TreinoAdapter;
import com.example.jota.androiddemo.app.Globals;
import com.example.jota.androiddemo.model.Treino;

import java.util.List;

public class TreinoListActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        this.getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_treino_list);
        Bundle extras = getIntent().getExtras();
        final Long idutilizador = extras.getLong("idutilizador");

        Globals.context = getApplicationContext();

        final ListView listTreinos = (ListView) findViewById(R.id.listtreinos);

        List<Treino> treinos = Globals.mgrTreino.getAllTreinoByDate();
        TreinoAdapter adapter = new TreinoAdapter(Globals.context, treinos);
        listTreinos.setAdapter(adapter);


        Button botaoPesqAvan = (Button) findViewById(R.id.btntreinospesqavan);
        botaoPesqAvan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle dataBundle = new Bundle();
                Intent intMenu = new Intent(Globals.context, SearchViewTreinoActivity.class);
                dataBundle.putLong("idutilizador", idutilizador);
                intMenu.putExtras(dataBundle);
                startActivity(intMenu);
            }
        });


        listTreinos.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {


                TextView datatv = ((TextView) view.findViewById(R.id.listTreinoData));
                TextView categoriatv = ((TextView) view.findViewById(R.id.listTreinoCategoria));
                TextView horatv = ((TextView) view.findViewById(R.id.listTreinoHora));
                TextView localidadetv = ((TextView) view.findViewById(R.id.listTreinoLocalidade));
                TextView campotv = ((TextView) view.findViewById(R.id.listTreinoCampo));

                String data = datatv.getText().toString();
                String categoria = categoriatv.getText().toString();
                String hora = horatv.getText().toString();
                String localidade = localidadetv.getText().toString();
                String campo = campotv.getText().toString();



                Treino treino = Globals.mgrTreino.getTreino(data, categoria, hora, localidade, campo);


                Bundle dataBundle = new Bundle();

                dataBundle.putString("observacao", treino.getObservacao());
                dataBundle.putString("campo", treino.getCampo());
                dataBundle.putString("data", treino.getData());
                dataBundle.putString("categoria", treino.getCategoria());
                dataBundle.putString("localidade", treino.getLocalidade());
                dataBundle.putString("hora", treino.getHora());
                dataBundle.putLong("idtreino", treino.getId());


                Intent intMain = new Intent(Globals.context, DadosTreinoActivity.class);
                intMain.putExtras(dataBundle);
                startActivity(intMain);
                finish();


            }
        });

    }


}
